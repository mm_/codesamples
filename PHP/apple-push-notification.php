<?php

class Push_Notification {

	protected $deviceToken;
	protected $passphrase = "passphrase";
	protected $message = "";
	protected $local_cert = "/path/to/cert/mytowns.pem";
	private $ctx;
	private $fp;
	private $gateway = 'ssl://gateway.sandbox.push.apple.com:2195';
	private $body = array();
	private $alert = "";

	function __construct($deviceToken,$message,$type,$uname,$uid,$extra){
		$this->deviceToken = $deviceToken;
		$this->startStream();
		$this->startAPNSConnection();
		$this->alert = $message;
		$this->setBody($message,'default',$type,$uname,$uid,$extra);
	}
	public function send(){
		return	$this->sendNotification();
	}
	private function startStream(){
		$this->ctx = stream_context_create();
		stream_context_set_option($this->ctx, 'ssl', 'local_cert', $this->local_cert);
		stream_context_set_option($this->ctx, 'ssl', 'passphrase', $this->passphrase);
	}

	private function startAPNSConnection(){
		$this->fp = stream_socket_client($this->gateway, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $this->ctx);

		if (!$this->fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		echo 'Connected to APNS' . PHP_EOL;
	}

	private function setBody($alert,$sound="default",$type,$uname,$uid,$extra){
		$this->body['aps'] = array( 

			'alert' => $alert, 
			//	'sound' => $sound,
			'type'  => $type,
			//	'time' => $uname,
			//	'uid'   => $uid,
			'extra' => $extra 
		);
	}

	public function sendNotification(){
		$payload = json_encode($this->body);

		$msg = chr(0) . pack('n', 32) . pack('H*', $this->deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($this->fp, $msg, strlen($msg));
		fclose($this->fp);
		if (!$result)
			return false;
		else
			return true;

	}
}


/// on new media uploaded or new comment or new friend frequest or new friend approve for all followers insert notification into notification table 
//  (user_id,text,create_time,read,sent,subject,meta)
//  cronjob will go through read,sent and sent email + push notification for each notification then mark as sent, 
$q = array();

/*		WEATHER NOTIFICATIONS			*/
$q['weather'] = "SELECT user_id,pushaccesstoken,text,type,create_time,notification_id,extra 
	FROM mt_users  
	RIGHT JOIN mt_user_notifications n 
	USING (user_id) 
	WHERE weather_notifications_enabled=1 AND char_length(pushaccesstoken)=64 AND n.sent=0 AND n.type='weather'";
/*		HEADLINES NOTIFICATIONS			*/
$q['headlines'] = "SELECT user_id,pushaccesstoken,text,type,create_time,notification_id,extra FROM mt_users  RIGHT JOIN mt_user_notifications n USING (user_id) WHERE town_headlines_notifications_enabled=1 AND char_length(pushaccesstoken)=64 AND n.sent=0 AND n.type='headlines'";
/*		SPONSORED NOTIFICATIONS			*/
$q['sponsored'] = "SELECT user_id,pushaccesstoken,text,type,create_time,notification_id,extra FROM mt_users  RIGHT JOIN mt_user_notifications n USING (user_id) WHERE sponsored_messages_enabled=1 AND char_length(pushaccesstoken)=64 AND n.sent=0 AND n.type='sponsored'";
/*		TREANDING TOPIC NOTIFICATIONS			*/
$q['trending'] = "SELECT user_id,pushaccesstoken,text,type,create_time,notification_id,extra FROM mt_users  RIGHT JOIN mt_user_notifications n USING (user_id) WHERE trending_topic_notifications_enabled=1 AND char_length(pushaccesstoken)=64 AND n.sent=0 AND n.type='trending'";
/*		SCHOOL CLOSING  NOTIFICATIONS			*/
$q['school_closing'] = "SELECT user_id,pushaccesstoken,text,type,create_time,notification_id,extra FROM mt_users  RIGHT JOIN mt_user_notifications n USING (user_id) WHERE school_closing_notifications_enabled=1 AND char_length(pushaccesstoken)=64 AND n.sent=0 GROUP BY user_id AND n.type='school_closing'";
foreach($q as $k=>$v){
	$a = mysql_query($v);
	if(count($a))
		sendNotifications($a,$k);
}
function sendNotifications($a,$type){
	foreach($a as $not ){
		$token = $not['pushaccesstoken'];
		$message = $not['text'];
		$uname = "";
		$type = $not['type'];
		$time = $not['create_time'];	
		$uid = $not['user_id'];
		$nid = $not['notification_id'];
		$extra = $not['extra'];
		$n = new Push_Notification($token,$message,$type,$time,$uid,$extra);
		if($n->send()){
			$q3 = "DELETE FROM mt_user_notifications WHERE notification_id = $nid";
			query_($q3);		
		}

	}
}
// notification will be marked to read once callback api is notified of user action
// read will be used for displaying updates within app
mysql_close($link);

?>
