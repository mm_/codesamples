<?php
class Vinport_Ajaxcart_IndexController extends Mage_Checkout_CartController{

	/*	Generate Block with and calculate number of items in cart
	 *	Return as JSON encoded array
	 */
	protected function setCartResponse(){
		Varien_Profiler::start(__METHOD__ . 'ajax_cart_display');
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$count = Mage::helper('checkout/cart')->getSummaryCount();
		$html = $this->getLayout()->createBlock('core/template');
		$html = $html->setTemplate('vinport/ajaxcart.phtml')->toHtml();
		$json = Mage::helper('core')->jsonEncode(array("body"=>$html,"count"=>$count)); 
		$this->getResponse()->setBody($json);
		Varien_Profiler::stop(__METHOD__ . 'ajax_cart_display');
	}
	public function indexAction()
	{
		$this->setCartResponse();
	}

	/*	quantity update posted,
	 *	qty = 0 => remove from cart
	 *	else update cart quantity
	 */ 
	public function updateAction(){
		$cart = Mage::helper('checkout/cart')->getCart();
		$quote = $cart->getQuote();
		$data = Mage::helper('core')->jsonDecode($this->getRequest()->getPost('update'));
		if(!count($data))
			return;
		$changed = false;
		foreach($data as $update){
			$pid = intval($update['id']);
			$qty = intval($update['value']);
			if($quote->hasProductId($pid)){
				$product = Mage::getModel('catalog/product')->load($pid);
				$item = $quote->getItemByProduct($product);
				if($qty!=$item->getQty()){
					//Remove Item if quantity is 0 else set the Qty on the item
					if($qty == 0 ){
						$cart->removeItem($item->getId());
					}else{
						$item->setQty($qty);
					}
					$changed = true;
				}
			}
		}
		if($changed){
			$cart->save();
			$this->_getSession()->setCartWasUpdated(true);
			$this->setCartResponse();
		}
	}

	/* remove item from cart by product ID
	 */
	public function removeAction(){
		$cart = $this->_getCart();
		$quote = $cart->getQuote();
		$pid = $this->getRequest()->getPost('id');
		$changed = false;
		if($quote->hasProductId($pid)){
			$product = Mage::getModel('catalog/product')->load($pid);
			$item = $quote->getItemByProduct($product);
			$changed = true;
			Mage::helper('checkout/cart')->getCart()->removeItem($item->getId())->save();
		}
		if($changed){
			$this->_getSession()->setCartWasUpdated(true);
			$this->setCartResponse();
		}
	}


	/* add item to cart by product id
	 */
	public function addAction()
	{
		$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();
		try {
			if (isset($params['qty'])) {
				$filter = new Zend_Filter_LocalizedToNormalized(
					array('locale' => Mage::app()->getLocale()->getLocaleCode())
				);
				$params['qty'] = $filter->filter($params['qty']);
			}

			/* Mage_Checkout_Controller->initProduct()
			 */
			$product = $this->_initProduct();
			/*
			 *	Check cart to prevent star trek wines from being ordered with regular products
			 *
			 */
			$products_cats = $product->getCategoryIds();
			$star_trek_category_id = 37;
			$error_msg = 'Star Trek Wines must be purchased separately from regular products.';
			$cart_products = $cart->getQuote()->getAllVisibleItems();
			if(count($cart_products)){
				$star_trek_in_cart = false;
				foreach($cart_products as $cart_product){
					if(in_array($star_trek_category_id,$cart_product->getProduct()->getCategoryIds())){
						//set to false if want mixed carts
						$star_trek_in_cart = false;
					}
				}
			}
			/**
			 * Check product availability
			 */
			if (!$product) {
				$this->_goBack();
				return;
			}

			$cart->addProduct($product, $params);
			if (!empty($related)) {
				$cart->addProductsByIds(explode(',', $related));
			}

			$cart->save();

			Mage::dispatchEvent('checkout_cart_add_product_complete',
				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
			);

			$this->setCartResponse();
		} catch (Mage_Core_Exception $e) {
			if ($this->_getSession()->getUseNotice(true)) {
				$this->_getSession()->addNotice($e->getMessage());
			} else {
				$messages = array_unique(explode("\n", $e->getMessage()));
				foreach ($messages as $message) {
					$this->_getSession()->addError($message);
					Mage::log($message);
				}
			}

			$url = $this->_getSession()->getRedirectUrl(true);
			if ($url) {
				$this->getResponse()->setRedirect($url);
			} else {
				$this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
			}
		} catch (Exception $e) {
			$this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
			Mage::logException($e);
			$this->_goBack();
		}
	}

}

