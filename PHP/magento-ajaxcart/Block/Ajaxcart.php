<?php

class Vinport_Ajaxcart_Block_Ajaxcart extends Mage_Core_Block_Template 
{
    public function _prepareLayout()
    {   
        $layout = parent::_prepareLayout();
        $this->setTemplate("vinport/ajaxcart.phtml");
        return $layout;

    }   

}
