<?php

/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Webpos Index Controller
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @author      Magestore Developer
 */
class Techmission_BMS_InventoryController extends Techmission_BMS_Controller_Action {

	public function updatepoAction() {
		$this->loadLayout();
		$this->renderLayout();
	}
	public function createpoAction() {

		$this->loadLayout();
		$this->renderLayout();
	}

    public function indexAction() {
		//Check if customer is accessing, should only be accessible to admin
		/*Test matt*/
		$this->loadLayout();
		return $this->renderLayout();
    }

}
