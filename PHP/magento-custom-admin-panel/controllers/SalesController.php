<?php

/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Webpos Index Controller
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @author      Magestore Developer
 */
class Techmission_BMS_SalesController extends Techmission_BMS_Controller_Action {

	public function returnsAction() {

		$this->loadLayout();
		$this->renderLayout();
	}

    /**
     * Additional initialization
     *
     */
    protected function _construct()
    {
    //    $this->setUsedModuleName('Mage_Sales');
    }

    public function indexAction()
    {
        $this->loadLayout();
     //   $this->_setActiveMenu('sales');

        $block = $this->getLayout()->createBlock('bms/sales', 'sales');
        $this->getLayout()->getBlock('content')->append($block);

    //    $this->_addBreadcrumb($this->__('Sales'), $this->__('Sales'));
    //    $this->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
        $this->renderLayout();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales');
    }

}
