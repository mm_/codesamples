<?php
/**
 * Techmission
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Techmission.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Techmission
 * @package     Techmission_BMS
 * @copyright   Copyright (c) 2012 Techmission (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * BMS Model
 * 
 * @category    Techmission
 * @package     Techmission_BMS
 * @author      Techmission Developer
 */
class Techmission_BMS_Model_Admin extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('bms/admin');
    }
}
