<?php
class Techmission_BMS_Block_Dashboard_Sales extends Mage_Adminhtml_Block_Dashboard_Grid_Container 
{
	public function __construct(){
	//	Mage::log("matt");
		$this->_blockGroup = "tehcmission_bms";
		$this->_controller = "bms_dashboard";
		$this->_headerText = Mage::helper('techmission_bms')->__("Sales");
		parent::__construct();
		$this->_removeButton('add');
	}

}
