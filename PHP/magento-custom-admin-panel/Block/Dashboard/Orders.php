<?php
class Techmission_BMS_Block_Dashboard_Orders extends Mage_Adminhtml_Block_Dashboard_Grid_Container
{
	public function __construct(){
		Mage::log("matt");
        $this->_controller = 'dashboard';
        $this->_blockGroup = 'bms';
		$this->_headerText = Mage::helper('techmission_bms')->__("Orders");
		parent::__construct();
		$this->_removeButton('add');
	}

}
