<?php

/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Webpos Block
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @author      Magestore Developer
 */
class Techmission_BMS_Block_Dashboard extends Techmission_BMS_Block_Widget {

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('bms/dashboard/index.phtml');
        $this->setId('product_edit');
    }

    protected function _prepareLayout(){
        $this->setChild('bms_dashboard_sale_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('New Sale'),
                    'onclick'   => "setLocation('".$this->getUrl('*/sales/index')."')",
                    // 'class'   => 'add'
                ))
        );
        $this->setChild('bms_dashboard_new_repair_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('New Repair'),
                    'onclick'   => "setLocation('".$this->getUrl('*/sales/repair')."')",
                    // 'class'   => 'add'
                ))
        );
        $this->setChild('bms_dashboard_new_return_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('New Return/Exchange'),
                    'onclick'   => "setLocation('".$this->getUrl('*/sales/returns')."')",
                    // 'class'   => 'add'
                ))
        );
        $this->setChild('bms_dashboard_new_po_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('New Purchase Order'),
                    'onclick'   => "setLocation('".$this->getUrl('*/inventory/createpo')."')",
                    // 'class'   => 'add'
                ))
        );
        $this->setChild('bms_dashboard_receive_po_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Receive Purchase Order'),
                    'onclick'   => "setLocation('".$this->getUrl('*/invetory/updatepo')."')",
                    // 'class'   => 'add'
                ))
        );
        $this->setChild('bms_dashboard_view_products_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('View Products'),
                    'onclick'   => "setLocation('".$this->getUrl('*/products/index')."')",
                    // 'class'   => 'add'
                ))
        );
    }


    public function getEmployeeHtml(){
        $admin = Mage::getSingleton('admin/session')->getUser();
        $username = $admin->getUsername();
        $firstname = $admin->getFirstname();
        $lastname = $admin->getLastname();
        $userid = $admin->getUserId();

        return "
        	<div class='employee_container'>
        	<span class='bms-dashboard-admin-info' >
		        Employee #<span class='bms-dashboard-admin-info-userid' >$userid</span>:&nbsp;
	            <span class='bms-dashboard-admin-info-username' >$username</span> &nbsp;
	            (<span class='bms-dashboard-admin-info-name' >$lastname, $firstname</span>)
	        </span>
	  </div>";

    }
    public function getQuickButtonsHtml(){
        return $this->getChildHtml('bms_dashboard_sale_button') .
                $this->getChildHtml('bms_dashboard_new_repair_button') .
                $this->getChildHtml('bms_dashboard_new_return_button') .
                $this->getChildHtml('bms_dashboard_new_po_button') .
                $this->getChildHtml('bms_dashboard_receive_po_button') .
                $this->getChildHtml('bms_dashboard_view_products_button');
    }

}
