<?php 

class Techmission_Bms_Block_Dashboard extends extends Mage_Core_Block_Template {
	protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('bms/dashboard.phtml');
        }
        return $this;
    }

}
