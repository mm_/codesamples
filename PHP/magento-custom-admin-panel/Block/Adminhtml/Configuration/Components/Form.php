<?php

/*
 * @copyright  Copyright (c) 2013 by  ESS-UA.
 */

class Techmission_BMS_Block_Adminhtml_Configuration_Components_Form extends Mage_Adminhtml_Block_System_Config_Form
{
    // ########################################

    public function __construct()
    {
        parent::__construct();

        // Initialization block
        //------------------------------
        $this->setId('configurationComponentsForm');
        //------------------------------

        $this->setTemplate('M2ePro/configuration/components.phtml');
    }

    // ########################################


    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'      => 'config_edit_form',
            'action'  => $this->getUrl('M2ePro/adminhtml_configuration_components/save'),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        //$this->getLayout()->getBlock('head')->addJs('M2ePro/Configuration/ComponentsHandler.js');
    }

    protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }

    // ########################################
}
