<?php

/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Webpos Block
 * 
 * @category    Magestore
 * @package     Magestore_Webpos
 * @author      Magestore Developer
 */
class Techmission_BMS_Block_Products extends Techmission_BMS_Block_Widget_Grid_Container {

 protected $_addButtonLabel = 'Add New Product';
 
    public function __construct()
    {
        parent::_construct();
        $this->_controller = 'products';
        $this->_blockGroup = 'bms';
		$this->setTemplate("bms/products/index.phtml");
        $this->_headerText = Mage::helper('bms')->__('Products');
        return  ;

    }

	protected function _prepareLayout()
   {
        $this->_addButton('add_new', array(
            'label'   => Mage::helper('catalog')->__('Add Product'),
            'onclick' => "setLocation('{$this->getUrl('*/*/new')}')",
            'class'   => 'add'
        )); 
		$this->setChild('bms_products_grid', $this->getLayout()->createBlock('bms/products_grid', 'bms.products.grid'));
       return parent::_prepareLayout();
   }

    public function getGridHtml()
    {
        return $this->getChildHtml('bms_products_grid');
    }

    public function getAddNewButtonHtml()
    {
            return $this->getLayout()->createBlock('bms/widget_button')
            ->setData(array(
                'label'     => Mage::helper('catalog')->__('Add Design Change'),
                'onclick'   => "setLocation('".$this->getUrl('*/*/new')."')",
                'class'   => 'add'
            ))->toHtml() ;
      //  return $this->getChildHtml('add_new_button');
			$this->setChild('add_new_button',
            $block = $this->getLayout()->createBlock('bms/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Add Design Change'),
                    'onclick'   => "setLocation('".$this->getUrl('*/*/new')."')",
                    'class'   => 'add'
                    ))  
                ); 
        return $block;
    }
}
