<?php
class Techmission_BMS_Block_Products_Import_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /** 
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {   
        parent::__construct();

        $this->removeButton('back')
            ->removeButton('reset')
            ->_updateButton('save', 'label', $this->__('Check Data'))
            ->_updateButton('save', 'id', 'upload_button')
            ->_updateButton('save', 'onclick', 'editForm.postToFrame();');
    }   

    /** 
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {   
        parent::_construct();

        $this->_objectId   = 'import_id';
        $this->_blockGroup = 'importexport';
        $this->_controller = 'bms_products';
    }   

    /** 
     * Get header text
     *
     * @return string
     */
    public function getHeaderText()
    {   
        return Mage::helper('importexport')->__('Import');
    }   
}
