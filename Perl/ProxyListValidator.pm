#!/usr/bin/perl

package ProxyListValidator;

use threads ('yield','stack_size'=>16*4096,'exit'=>'threads_only','stringify');
use Thread::Queue;
use threads::shared;

use strict;
use warnings;

#begin prompting user for information
	sub new 
	{
		my $type = shift;            # The package/type name
		my $thread_queue=Thread::Queue->new();

		my $self = {
			_input_file 	=>	shift, 
			_output_file	=>  shift,
			_url_to_validate=> 	shift, 
			_timeout		=> 	shift,
			_good_ip_array 	=>	[],
			_bad_ip_array	=>	[],
			_command_array 	=> 	[],
			_lines			=>	[],
			_count_processed=>	 0,
			_thread_queue	=>	[Thread::Queue->new()]
		};
		#concurrency
		share($self->{_count_processed});
		share($self->{_good_ip_array});
		share($self->{_bad_ip_array});
		bless $self,$type;
		return $self; 
	}



		sub process { 
			my ($self,%args) = @_;
			read_input_file(@_);
			prepare_command_array(@_);
			$self->{_thread_queue}->[0]->enqueue(@{$self->{_command_array}});
			#create threads
			#start running threads
			$_->join for (create_thread_map(@_));
			output_valid_proxies(@_);
			print "\nFinished.\nTotal Working Proxies: " . @{$self->{_good_ip_array}};
			
		}

		sub create_thread_map {
			my ($self,%args) = @_;
			my @arr = (1,2,3,4);
			my @thread_map;
			foreach(@arr){
				push(@thread_map,threads->create(\&run,@_));
			}
			return @thread_map;
		}

		sub run { 
			my ($self,%args) = @_;
			my $c=0;
			my ($job_id,$s);
			while(my $command = $self->{_thread_queue}->[0]->dequeue_nb()){
				($job_id,$s)=split/,/,$command;
				open(my $nem,"$command|");
				my @out = ();
				while(my $linee = <$nem>){
					push(@out,$linee);
				}
				$_ = $command;
				if( /-x ([0-9:\.]+)/){
					$command=$1;
				}
				validate_response(@_,$command,@out);
				close($nem);
				update_count(@_);
				print "\r" . $self->{_count_processed}. " / " . @{$self->{_lines}} ;
			}
			return;
		}
		sub update_count {
			my ($self,%args) = shift;
			$self->{_count_processed} ++;
		}


		sub validate_response {
			my ($self,%args) = shift;
			my $command = shift;
			my @response = shift;
			#use first line of response (HTTP Code header)
			$_ = $response[0];
			#HTTP Response 200 for valid
			if(length($response[0]) && /200 OK/){
				push(@{$self->{_good_ip_array}},$command);}
			else{
				push(@{$self->{_bad_ip_array}},$command);
			
			}
		}

		#open and read proxy list file
		sub read_input_file {
			my ($self,%args) = @_;
			open (DAT, $self->{_input_file});
			@{$self->{_lines}} = <DAT>;
			close (DAT);
		}

		sub prepare_command_array {
			my ($self,%args) = @_;
			my ($temp, $command);
			foreach $temp(@{$self->{_lines}}){
				chomp($temp);
				#mac return line removal
				$temp =~ s/\r|\n//g;
				$command = get_curl_command(@_,$temp); 
				#push onto command array
				push(@{$self->{_command_array}},$command);
			}
		}

		sub get_curl_command { 
			my ($self,%args) = shift;
			my $proxy = shift;
			my $pr = " -x ".$proxy." ";
			my $curl ="curl -I -m ".$self->{_timeout}." -s ";
			my $command = $curl.$pr." \"".$self->{_url_to_validate}."\" "; 
			return $command;
		}

		sub output_valid_proxies{
			my ($self,%args) = shift;
			my $proxy;
			open (FH, ">>" . $self->{_output_file} );
			foreach $proxy (@{$self->{_good_ip_array}}){
				print FH $proxy ."\n";
			}
			close FH;
		}

1;
