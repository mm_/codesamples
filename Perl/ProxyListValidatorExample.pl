#!/usr/bin/perl

use ProxyListValidator;


#prompt user terminal for info 

        print "What Proxy List File to use?";
        #proxy list (tab separated IP, port on each line)
        my $data_file = <>; 
        #remove newlines 
        chomp($data_file);

        print "What file to save to?";
        my $output_file = <>; 
        chomp($output_file);

        print "Enter url to check against:";
        my $url_to_validate = <>; 
        chomp($url_to_validate);

    
        print "Timeout in seconds:";
        my $timeout = <>; 
        chomp($timeout);

#create Class
my $Validator = new ProxyListValidator ( $data_file,
										$output_file,
										$url_to_validate,
										$timeout);
$Validator->process();
