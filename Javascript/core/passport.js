var passport_groups = function () {
	Event.observe(window, 'load', function () {	
		var badges = $$('.passport_stamps li');
		var groups = [];
		
		
		var isIE = /*@cc_on!@*/0;

		if (!isIE) {
			badges.each(function(badge) {
				badge.group = badge.readAttribute('data-group');
				if (badge.group !== null) {
					if (!groups[badge.group]) {
						groups[badge.group] = [];
					}
				
					groups[badge.group].push(badge);
				}
			
				if (badge.innerHTML == '') {
					return;
				}
			
				var dT = new DescriptionToggler(badge),
					 img = badge.select('img')[0];

				img.observe('mouseover', function (e) {
	 				e.stop(); 
					dT.on(this);
				});
		
				img.observe('mouseout', function (e) {
	 				e.stop(); 
					dT.off();
				});
		
			});
		}
		
		groups.each(function(group) {
			if (group.length <= 1) {
				return;
			}
			
			var i = 1;
			var container = makeTemp();
			var prev = group[0].previous();
			var complete;
			
			group.each(function(badge) {
				container.down('div[data-insert="li"]').insert(badge);
				if (badge.readAttribute('data-complete') == 1) {
					complete = i;
				}
				i++;
			});
			
			prev.insert({after: container});
			
			var slider = new Slider(container);

			if (complete > 1) {
				slider.slide(complete);
			}
			
			var circles = container.select('.passport_little_circle');
			var i = 1;
			circles.each(function (circle) {
			
				if (group.length < i) {		// Get rid of one of the circles, if it's a two-tile group
													// And change the CSS offset to compensate and center it
					var circleWidth = $(circle).getStyle('width');
						 circleParent = circle.ancestors()[0],
						 parentWidth = circleParent.getStyle('width'),
						 newWidth = null, newOffset = null;				
					
					circleWidth = parseInt(circleWidth) + (parseInt(circle.getStyle('marginLeft')) * 2);
					newWidth = parseInt(parentWidth) - parseInt(circleWidth);
					circleParent.setStyle({width: newWidth + 'px'});
					
					newOffset = 0-(newWidth/2);					
					circleParent.setStyle({marginLeft: newOffset + 'px'});
					
					circle.remove();
				}
				
				circle.observe('click', function () {
					var num = circle.readAttribute('data-num')
					slider.slide(num, this);
				});
				i++;
			});
		});


		var bookmarkLinks = $$('#passport_bookmark li');
		bookmarkLinks.each(function (elem) {
			elem.observe('click', function (e) {
				e.stop();
				bookmarkLinks.each(function (elem) {
					elem.removeClassName('bookmark_selected');
				});
				this.addClassName('bookmark_selected');
				
				var category = this.readAttribute('data-category');
				badges.each(function (badge) {
					var badgeCategory = badge.readAttribute('data-category');
					if (!(badgeCategory == category || category == 'all')) {
						badge.setStyle({display: 'none'});
						var viewport = badge.up('.passport_group_viewport');
						if (viewport) {
							var anythingShowing = false;
							viewport.select('li').each(function(o) {
								if (o.getStyle('display') != 'none') {
									anythingShowing = true;
								}
							});
							
							if (!anythingShowing) {
								viewport.setStyle({display: 'none'});
							}
						}
					} else {
						badge.setStyle({display: 'block'});
						var viewport = badge.up('.passport_group_viewport');
						if (viewport) {
							var anythingShowing = false;
							viewport.select('li').each(function(o) {
								if (o.getStyle('display') != 'none') {
									anythingShowing = true;
								}
							});
							
							if (anythingShowing) {
								viewport.setStyle({display: 'block'});
							}
						}
					}
				});
			})
		});

	});
		
	
	// Returns a blank copy of #passport_template
	var makeTemp = function () {		
		var template = $('passport_template').innerHTML;
		
		return function () {
			var div = new Element('div');
			var elem = div.insert(template);
			elem = elem.childNodes[1];
			return elem;
		};
	}();
	
	var Slider = function (elem) {
		var slideElem = elem.select('.passport_group_slider')[0];
		var currentClass = 'tile1';
		var circles = elem.select('.passport_little_circle');
		var vignette = elem.select('.passport_slider_vignette')[0];
		return {
			slide: function (num, circle) {
				if (circle == null) {
					var circle;
					circles.each(function (c) {
						var data = c.readAttribute('data-num')
						if (data == num) {
							circle = c;
						}
					});
				}
				
				vignette.addClassName('passport_vignette_show');
				
				$(slideElem).removeClassName(currentClass);
				var newClass = 'tile' + num;
				slideElem.addClassName(newClass);
				currentClass = newClass;
				
				circles.each(function (circle) {
					$(circle).removeClassName('selected');
				});
				
				circle.addClassName('selected');
				
				window.setTimeout(function () {
					vignette.removeClassName('passport_vignette_show');
				}, 1000);
			}
		}
	};
	
	var DescriptionToggler = function (elem) {
		var description = elem.select('.passport_stamp_text')[0];
		if (!description) {
			return;
		}
		description.remove();
		var passport_parent = $('passport_desc')
		var ul = $$('.passport_stamps')[0];	
		var timer = null, timer2 = null;
		var mouseIsOver = false;
		
		var getOffsetFromUL = function (elem) {
			var oP;
			var i = 0;
			while (elem != ul && i < 100) {
				oP = elem;
				elem = elem.getOffsetParent();
				i++;
			}
			return oP.positionedOffset();
		}
				
		return {
			on: function (o) {
				mouseIsOver = true;
				window.clearTimeout(timer);
				window.clearTimeout(timer2)
				offset = getOffsetFromUL(o);
				description.setStyle({display: 'block', opacity: 0});
				description.setStyle({left: offset['left'] + 'px', top: offset['top'] + 'px'});
				passport_parent.insert(description);
				window.setTimeout(function () {
					description.setStyle({opacity: 1})
				}, 5);
			},
			
			off: function () {
				timer = window.setTimeout(function () {
					description.setStyle({opacity: 0});
					timer2 = window.setTimeout(function () {
						description.remove();
					}, 500);
				}, 100);
			}
		}
	}
	
}();
