document.observe("dom:loaded",function(){
	Event.observe($$("#util_nav .cart")[0],'mouseenter',function(evt){
		//make sure popunder is populated and not already visible
		if($$(".cart_popunder")[0].innerHTML.length>0 && $$(".cart_popunder")[0].style.display!="")
		Effect.toggle($$(".cart_popunder")[0],'Slide',{duration:1});
	}); 
	Event.observe($$("li.cart")[0],'mouseleave',function(evt){
		if($$(".cart_popunder")[0].innerHTML.length>0 && $$(".cart_popunder")[0].style.display!="none")
		Effect.toggle($$(".cart_popunder")[0],'Slide',{duration:1});
	}); 
});

var loaded = false;
function startLoading(){
	loaded = false;
	window.setTimeout('showLoadingImage()', 10);
}
function showLoadingImage(){
	var el = document.getElementById("loading_box");
	if (el && !loaded) {
		el.innerHTML = '<img src="/skin/frontend/default/modern/images/loading.gif">';
	}
	new Effect.Appear('loading_box');
}
function stopLoading(){
	Element.hide('loading_box');
	loaded = true;
	Ajax.Responders.register({
		onCreate : function(){},
		onComplete : function(){}
	});
}

function ajax_failure(){
	alert("Something went wrong with adjusting Cart");
}
function ajax_add_to_cart(url){
	var pos = url.indexOf("checkout/cart");
	url = url.substr(pos).replace("checkout/cart","/index.php/ajaxcart/index");
	Ajax.Responders.register({
		onCreate : startLoading,
		onComplete : stopLoading
	});
	new Ajax.Request(url, {
		method: 'get',
		onSuccess: function(response) {
			process_ajax_response(response);
		},
		onFailure: ajax_failure
	});
}
function process_ajax_response(response){
	var json = response.responseText.evalJSON();
	if(json.body)
		$$(".cart_popunder")[0].replace(json.body);
	if(typeof json.count != "undefined"){
		if(json.count > 0){
			$$(".util_nav_cart_count")[0].update(json.count);
			$$(".util_nav_cart_count")[0].style.display = "";
			Effect.toggle($$(".cart_popunder")[0],'Slide',{duration:1});
			Element.scrollTo( $$(".util_nav_cart_count")[0]);
			setTimeout(function(){
				Effect.toggle($$(".cart_popunder")[0],'Slide',{duration:1});
			},50);
		}else{
			$$(".util_nav_cart_count")[0].update(0);
			$$(".util_nav_cart_count")[0].style.display = "none";
		}
	}

}
function ajax_remove_item(pid){
	var values = {"id":pid};
	var url = "/ajaxcart/index/remove";
	Ajax.Responders.register({
		onCreate : startLoading,
		onComplete : stopLoading
	});
	new Ajax.Request(url, {
		method: 'post',
		parameters: values,
		onSuccess: function(response) {
			process_ajax_response(response);
		},
		onFailure: ajax_failure
	});
}	
var matt ='';
function ajax_update_cart(){
	var values = [];
	var cart = $$('.cart_popunder input');
	var i = 0;
	cart.each(function(val){
		if(val.name.match(/qty_[0-9]+/g)){
			var id = val.name.replace("qty_","");
			var value = val.value;
			var upd = {}; 
			upd.id = id;
			upd.value= value;
			values.push(upd);
		}
	});
	if(values.length <1)
		return;
	var url = "/ajaxcart/index/update";
	Ajax.Responders.register({
		onCreate : startLoading,
		onComplete : stopLoading
	});
	new Ajax.Request(url, {
		method: 'post',
		parameters: "update="+Object.toJSON(values),
		onSuccess: function(response) {
			process_ajax_response(response);
		},
		onFailure: ajax_failure
	});
}

