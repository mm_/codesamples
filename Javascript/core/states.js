var allowed_states = 	["Arizona","California","Colorado","Connecticut","District of Columbia","Florida",
						"Georgia","Idaho","Illinois","Indiana","Iowa","Kansas","Louisiana","Maine","Maryland","Massachusetts",
						"Michigan","Minnesota","Missouri","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico",
						"New York","North Carolina","Ohio","Oregon","South Carolina","Tennessee","Texas","Vermont",
						"Virginia","Washington","West Virginia","Wisconsin","Wyoming"];

var nP = navigator.platform;
if (nP == "iPad" || nP == "iPhone" || nP == "iPod" || nP == "iPhone Simulator" || nP == "iPad Simulator"){
    var mobileSafari = true;
} else {
	var mobileSafari = false;
}
if ($('billing:region_id')) {	// checkout page
	if($('shipping-address-select')){	
		$('shipping-address-select').on('change', function () {
			if (this.value == '') {
				$$('input').each(function (o) {
					if (o.id.indexOf('shipping') == 0) {
						o.value = '';
					}
				});
			}
		});
	}
	if($('billing-address-select')){	
		$('billing-address-select').on('change', function () {
			if (this.value == '') {
				$$('input').each(function (o) {
					if (o.id.indexOf('billing') == 0) {
						o.value = '';
					}
				});
			}
		});
	}
	
var first_load= true;	
	Event.observe (window, 'load', function() {
		toggleRadioState(stateAllowed($$('select[name=billing\[region_id\]] option')[$$('select[name=billing\[region_id\]]')[0].selectedIndex].innerHTML));
	first_load = false;
		// window.setTimeout(function () {makeSureTheStupidStatesAreDisabled(); }, 500);
	});
	window.setTimeout(function () {makeSureTheStupidStatesAreDisabled(); }, 500);
	

	// In the shipping section, disable states we can't ship to
	Event.observe ('shipping:region_id', 'focus', function() {
		var options = $$('select[name="shipping\[region_id\]"] option');
		if (this.value && options[this.value]) {
			var selected_state = options[this.value].innerHTML;
		
			if (!stateAllowed(selected_state)) {
				this.value = '';
			}
		} else {
			this.value = '';
		}
		
		$$('select[name="shipping\[region_id\]"] option').each(function(o) {
			if (!stateAllowed(o.innerHTML)) {
				o.remove();
				//o.disabled = true;
			}
		});	
	});
	
	// Same thing, but for touch devices listen for touchstart and remove the elements.
	Event.observe ('shipping:region_id', 'touchstart', function() {
		$$('select[name="shipping\[region_id\]"] option').each(function(o) {
			if (!stateAllowed(o.innerHTML)) {
				o.remove();
			}
		});	
	});
	


	// Don't let the user choose "ship to billing address" if said address is in a place we can't ship to!!

	//Countries first.
	Event.observe ('billing:country_id', 'change', function() {
		toggleRadioState(this.value == 'US'); 
	});


	// And now states...
	Event.observe ('billing:region_id', 'change', function() {
		var options = $$('select[name="billing\[region_id\]"] option'),
		 	selected_state = options[this.value].innerHTML;
		
		toggleRadioState(stateAllowed(selected_state));
	});


	// Check out the addresses from the dropdown...
	Event.observe ('billing-address-select', 'change', function() {
		var addressID = this.selectedIndex,
		 	address = this.childNodes[addressID].innerHTML;

		toggleRadioState(matchAddress(address));
	});


	// Make it happen as soon as the page loads...
	Event.observe (window, 'load', function() {
		// "billing" list
		if( $('billing-address-select')!== null){
		var e = $('billing-address-select'),
		 	addressID = e.selectedIndex,
		 	address = e.childNodes[addressID].innerHTML;
	
			toggleRadioState(matchAddress(address));
		}
	
	
		// The countries box should be disabled, and set to US
		// Magento is stupid, so make a *fake* 'country' select box to disable, and hide the real one.
		$('shipping:country_id').value = 'US';
		$('shipping:country_id').hide();
		$('shipping:country_id').insert({after: '<select disabled style="width: 234px;">' +
												'<option>United States</option>' +
												'</select>'});

	
		// "shipping" list
		$$('select[name="shipping_address_id"] option').each(function(o) {
			if (!matchAddress(o.innerHTML) && o.innerHTML != 'New Address') {
				o.disabled = true;
				if (mobileSafari) {
					o.remove();
				}
				if (o.selected) {
					o.selected = false;
				}
			}
		});
	
		// If there is not a single valid address, show the new address form
		if( $('shipping-address-select')!== null){
		var index = $('shipping-address-select').selectedIndex,
			test = $('shipping-address-select').childNodes[index].innerHTML;
			
			if (test == 'New Address') {
				shipping.newAddress(true);
				shippingRegionUpdater.update();
			}
		}
	});
	
	
} else if ($('region_id')) {	// edit addresses page
	if ($('primary_shipping')) {	// general address page
		Event.observe ('country', 'change', function() {
			toggleCheckState(this.value == 'US'); 
		});
		
		Event.observe ('region_id', 'change', function() {
			var options = $$('select[name="region_id"] option'),
			 	selected_state = options[this.value].innerHTML;
			toggleCheckState(stateAllowed(selected_state));
		});
		
		Event.observe (window, 'load', function() {
			toggleCheckState($('country').value == 'US');
			$('country').insert({after: '<select disabled style="width: 234px;" id="fake_country">' +
										'<option>United States</option>' +
										'</select>'});
			$('fake_country').hide();
			
			var options = $$('select[name="region_id"] option');
			if ($('region_id').value && options[$('region_id').value]) {
				selected_state = options[$('region_id').value].innerHTML;
				toggleCheckState(stateAllowed(selected_state));
			}
		});
		
		Event.observe ('primary_shipping', 'change', function() {
			e = $('primary_shipping').checked;
			
			if (e) {
				$$('select[name="region_id"] option').each(function(o) {
					if (!stateAllowed(o.innerHTML)) {
						o.disabled = true;
						if (mobileSafari) {
							o.remove();
						}
					}
				});
				$('country').value = 'US';
				$('country').hide();
				$('fake_country').show();
			} else {
				$$('select[name="region_id"] option').each(function(o) {
					if (o.disabled) {
						o.disabled = false;
					}
				});
				
				$('fake_country').hide();
				$('country').show();
			}
			
		});

	} else if ($('default_shipping_hidden')){
		$('region_id').observe('change', function () {
			var options = $$('select[name="region_id"] option');
			toggleHiddenValue(stateAllowed(options[this.value].innerHTML));
		});
		
		Event.observe ('country', 'change', function() {
			toggleHiddenValue(this.value == 'US'); 
		});
		
		
	} else {	//default shipping page
		// disable the states we can't ship to
		Event.observe ('region_id', 'focus', function() {
			var options = $$('select[name="shipping\[region_id\]"] option');
			if (this.value && options[this.value]) {
			 	selected_state = options[this.value].innerHTML;
			
				if (!stateAllowed(selected_state)) {
					this.value = '';
				}
			}
			
			$$('select[name="region_id"] option').each(function(o) {
				if (!stateAllowed(o.innerHTML)) {
					o.disabled = true;
				}
			});	
			Event.stopObserving('shipping:region_id', 'focus');
		});
		
		Event.observe ('region_id', 'touchstart', function() {
			$$('select[name="region_id"] option').each(function(o) {
				if (!stateAllowed(o.innerHTML)) {
					o.remove();
				}
			});	
			Event.stopObserving('shipping:region_id', 'focus');
		});
		
		
		// disable the 'country' select
		Event.observe (window, 'load', function() {
			// The countries box should be disabled, and set to US
			// Magento is stupid, so make a *fake* 'country' select box to disable, and hide the real one.
			$('country').value = 'US';
			$('country').hide();
			$('country').insert({after: '<select disabled style="width: 234px;" id="fake_country">' +
										'<option>United States</option>' +
										'</select>'});
		});
	}
}


function matchAddress(address) {
	var allowed = false;
	if (address == 'New Address') return;

	for (i=0; i<allowed_states.length; i++) {
		regex = new RegExp(', ' + allowed_states[i] + ' (,?)[a-zA-Z0-9\-]+?, United States$');
		if (address.match(regex)) {
			allowed = true;
			break;
		}	
	}
	return allowed;
}

var chosen = false;
function toggleRadioState(e) {
	var yes = $('billing:use_for_shipping_yes');
	 	no = $('billing:use_for_shipping_no'),
	 	check = $('shipping:same_as_billing');
	
	if (!e) {
		if (yes.disabled) return; 
		check.checked = false;
		yes.disabled = check.disabled = true;
		if(!first_load){
			$$("label[for='billing\:use_for_shipping_yes']")[0].insert({"after":'<p class="validation-advice billing-state-error">This state is not available as a shipping destination.  You may select a different state under Shipping information.  Please check out the <a href="/shipping-returns" target="_blank" >"Shipping"</a> tab at the bottom of the screen for full details.</p>'});
		}
		if (yes.checked) {
			yes.checked = false;
			no.checked = chosen = true;
		} else {
			chosen = false;
		}
	} else {
		if($$(".billing-state-error").length){
			$$(".billing-state-error")[0].remove();
		}
		if (!yes.disabled) return;
		yes.disabled = check.disabled = false;
		if (chosen) {
			no.checked = false;
			yes.checked = true;
		}
	}
}

function toggleCheckState(e) {
	var check = $('primary_shipping');
	if (check) {
		if (!e) {
			if (check.disabled) return;
			check.disabled = true;
			if (check.checked) {
				chosen = true;
				check.checked = false;
			} else {
				chosen = false;
			}
		} else {
			if (!check.disabled) return;
			check.disabled = false;
			if (chosen) {
				check.checked = true;
			}
		}
	}
}

function toggleHiddenValue (e) {
	var hidden = $('default_shipping_hidden');
	if (hidden) {
		if (!e) {
			hidden.value = '0';
		} else {
			hidden.value = '1';
		}
	}
}


function stateAllowed(e) {
	var allowed = false;
	
	for (var i = 0; i < allowed_states.length; i++) {
		if (e == allowed_states[i]) {
			allowed = true;
			break;
		}
	}
	return allowed;
}

function shippingButtonContinueClicked() {
	if($('shipping-address-select')){
	var index = $('shipping-address-select').selectedIndex,
		test = $('shipping-address-select').childNodes[index].innerHTML;
	}else{
		var test = "New Address";
	}
	if ( test == 'New Address') {
		var state_select = $('shipping:region_id');
		var options = $$('select[name="shipping\[region_id\]"] option');
	
	
		if (!state_select || !state_select.value || !options || !options[state_select.value]) {
			shipping.save();
			return;
		}
		var selected_state = options[state_select.value].innerHTML;
	
		if (!stateAllowed(selected_state)) {
			alert('We aren\'t allowed to ship wine to ' + selected_state + '. Sorry for the invonvenience!');
//			Event.stop(event);
			// checkout.back();
		} else {
			shipping.save();
		}
	} else {
		if (matchAddress(test)) {
			shipping.save();
		} else {
			alert('We can\'t ship to your state. Sorry for the inconvenience!');
			shipping.newAddress(true);
			shippingRegionUpdater.update();
			makeSureTheStupidStatesAreDisabled();
		}
	}
}
function billingButtonContinueClicked(){
	//check if "Ship to this address" was selected 
	if($('billing:use_for_shipping_yes').checked == true){
		//auto populate shipping form;
		shipping.setSameAsBilling(true);
		shipping.save();
	}
}
function makeSureTheStupidStatesAreDisabled () {
	var state_select = $('shipping:region_id'),
	 	options = $$('select[name="shipping\[region_id\]"] option');

	if (state_select.value && options[state_select.value]) {
		var selected_state = options[state_select.value].innerHTML;
		if (!stateAllowed(selected_state)) {
			state_select.value = '';		
		}
	} else {
		state_select.value = '';
	}

	$$('select[name="shipping\[region_id\]"] option').each(function(o) {
		if (!stateAllowed(o.innerHTML)) {
			o.disabled = true;
		}
	});
}

