// (function ($) {
  var MT = {};
  
  MT.model = function () {
    
    // BASE API URL
    var API_URL = '/api/post/';
    var MEDIA_URL = '/uploads/';
    
    var Models = {
    
      // Generic 'post' class. Is extended later to
      // become 'report' and 'comment'
      Post: Backbone.Model.extend({
      
        // Turn off *actually* deleting files.
        // Used for testing the interface.
        enableDelete: true,
        
        // Update the status of the post.
        // Values: approve, deny, set/unset featured, or delete
        // Used setApproved, setFeatured, etc to call this
        alter: function (act, callback) {
          var data_type = this.get('type') + '_id';
          var data = {};
          data[data_type] = this.id;
          
          var that = this;
          $.ajax({
            type: 'POST',
            url: API_URL + 'admin/' + act + '/' + this.get('type') + '/',
            data: data,
            
            success: function (responseText) {
              try {
                var response = $.parseJSON(responseText);
              } catch (e) {
              
                // If the response is not JSON, it's probably a PHP error.
                // Show the generic error message
                console.log(responseText);
                MT.view.modal.show('Error');
              }
              
              if (response && !response.error) {
                callback();
              } else {
                MT.view.modal.show('error');
                response && console.error(response);
              }
            }
          });
        },
        
        
        // Mark a post as 'approved', or 'unapproved'
        setApproved: function (test) {
          
          // If a post is not approved, it cannot be featured
          if (!test && this.get('featured') == '1') {
            this.setFeatured(false);
          }
          
          var act = test ? 'approve' : 'deny';
          
          var that = this;
          this.alter(act, function () {
            that.set({status: test ? 'approved' : 'denied'});
          });
        },
        
        // Set or unset 'featured' status
        setFeatured: function (test) {
          
          // If it is to be featured, it must be approved
          if (test && this.get('status') !== 'approved') {
            this.setApproved(true);
          }
          
          var set = test ? 'set' : 'unset';
          var act = set + '/featured';
          
          var that = this;
          this.alter(act, function () {
            that.set({featured: test ? '1' : '0'});
          });
        },
        
        // Set or unset 'selected' status (for export)
        setSelected: function (selectForExport) {
          this.set({selected: selectForExport ? '1' : '0'});
        },
        
        // Delete the post. 
        destroy: function () {
        
          // If delete is disabled, just trigger the animation
          if (!this.enableDelete) {
            this.trigger('destroy');
            return;
          }
          
          var that = this;
          this.alter('delete', function () {
            that.trigger('destroy');
          });
        },
        
        // Get any picture or video associated with a report
        getMedia: function () {
          var media = {};
          
          if (this.get('image')) {
            media.image = MEDIA_URL + this.get('image');
          }
          
          if (this.get('thumbnail')) {
            media.thumbnail = MEDIA_URL + this.get('thumbnail');
          }
          
          if (this.get('video')) {
            media.video = MEDIA_URL + this.get('video');
          }
          
          return media ? media : false;
        },
        
        // Get all the comments associated with the report
        getComments: function () {
          if (this.commentsLoaded) {
            return;
          }
          
          if (parseInt(this.get('comment_count'), 10) == 0) {
            return;
          }
          
          this.set({
            comments: new Collections.Comments(this.id)
          });
          
          this.commentsLoaded = true;
        },
        
        // Set up the events to update the view.
        initialize: function () {
          this.on('change:status', function () {
            this.view.updateStatus(this.get('status'));
          });
          
          this.on('change:featured', function () {
            this.view.updateStatus('featured');
          });
          
          this.on('change:selected', function () {
            this.view.updateStatus('selected', this);
          });
          
          this.on('destroy', function () {
            this.view.remove();
          });
        }
      }),
      
      
      // Model that represents Twitter hashtags and fetches Tweets. 
      Twitter: Backbone.Model.extend({
        initialize: function () {
          this.tweets = {};
          var that = this;
          var isInitialLoad = true;
          
          // Get the trending hashtag.
          $.getJSON(
            API_URL + 'admin/get/trending/hashtag/',
            function (data) {
              if (!data.error) {
                that.set({tag: data.hashtag});
              } else {
                console.log(data.error_message);
              }
            }
          );
          
          // The changing of the hashtag
          this.on('change:tag', function () { 
            
            var tag = this.get('tag');
            var that = this;
            
            // I put the 'hash' in 'hashtag'
            if (tag.charAt(0) != '#') {
              this.set('tag', '#' + tag, {silent: true});
            }
            
            var populateHastag  = function() {
              isInitialLoad = false;
              // Update the view
              MT.view.twitter.updateTag();
                  
              // Update the list of tweets
              that.getTweets();
            };
            
            // don't set the hastag if we're merely opening the section
            // otherwise we send a push notification even if it didn't change!
            
            if (isInitialLoad) {
              populateHastag();
            } else {
              // Update the server
              $.post(
                API_URL + 'admin/set/trending/hashtag/',
                {hashtag: this.get('tag')},
                function (data) {
                  if (!data.error) {
                    populateHastag();
                  } else {
                    console.log(data.error);
                    MT.view.modal.show('error');
                  }
                }
              );
            }
            
          });
          
          
          // Update the view when new tweets are downloaded
          this.on('change:tweets', function () {
            MT.view.twitter.updateTweets();
          });     
        },
        
        
        // Use the twitter API to find tweets with the trending hashtag
        getTweets: function () {
          var that = this;
          
          $.ajax({
            type: 'POST',
            url: 'http://search.twitter.com/search.json',
            data: {
              q: this.get('tag')
            },
            dataType: 'jsonp',
            
            success: function (data) {
              that.set({tweets: data.results});
            },
            
            error: function (data) {
              console.log(data);
              that.set({tweets: null});
              MT.view.modal.show('error');
            }
          });
        }
      }),
      
      
      // The "export" model. Gets CSV files.
      // This isn't really a "model" but I put it here for consistency.
      // Choices are 'reports' and 'users'
      Export:  function () {
        return {
          downloadCSV: function (type) {
            var url;

            if (type == 'users' || type == 'reports' || type == 'report') {
              url = API_URL + 'admin/download/' + type + '/csv/';
              
              // report needs a POST var
              if (type == 'report') {
                // /report/ 
                // '12,34,56,78'
                
              } else {
                // Load the report in an IFrame. The brower will download it,
                // but it won't try to redirect to another page
                $('#report_frame').attr('src', url);
              }
            }
          }
        };
      },
      
      Sponsored: Backbone.Model.extend({
        initialize: function () {         
          var that = this;
          $.get(API_URL + 'admin/get/towns/', function (data) {
            try {
              data = $.parseJSON(data);
              var towns = [];
              _.forEach(data.towns, function (item) {
                towns.push(item);
              });
              
              that.set({towns: towns});
              
              MT.view.sponsored.insertTowns();
            } catch (e) {
              MT.view.modal.show('error');
              console.log(e);
            }
          });
          
          
          this.on('change', function () {
            if (this.hasChanged('towns')) {
              return;
            }
            
            var data = {
              message: this.get('message'),
              set_time: this.get('set_time')
            };
            
            var url;
            
            if (this.get('town_id') == 'all') {
              url = API_URL + 'admin/set/sponsored/message/all/';
            } else {
              url = API_URL + 'admin/set/sponsored/message/';
              data.town_id = this.get('town_id');
            }
            
            if (this.get('url')) {
              data.url = this.get('url');
            }
            
            console.log(data);
            
            $.ajax({
              type: 'POST',
              url: url,
              data: data,
              success: function (response) {
                try {
                  response = $.parseJSON(response);
                } catch (e) {
                  MT.view.modal.show('error');
                  console.error(response);
                  return;
                }
                
                if (response.error) {
                  MT.view.modal.show('error');
                  console.log(response);
                  return;
                }
                
                MT.view.modal.show('sponsored_success');
              }
            });
          });
        }
      })
    };
    
    // Set up the two collections
    var Collections = {
      Reports: Backbone.Collection.extend({
        idAttr: 'report_id',
        type: 'reports',
        singleType: 'report',
        model: Models.Post,
        loaded: false,
        loading: false,
        offset: 0,
        page: 10,
        
        // Get all the posts when the model is initialized.
        initialize: function () {
          
          this.loadPosts();
          
          // Update the view when posts load
          this.on('load:reports', function () {
            this.loaded = true;
            MT.view.reports.renderPosts();
          });
        },
        
        loadPosts: function () {
          if (this.loading) {
            return;
          }
          
          this.loading = true;
          var that = this;
          
          var req = $.post(API_URL + 'admin/get/' + this.type + '/', 
                  {start: that.offset},
                  function (data) {
                    try {
                      data = $.parseJSON(data);
                    } catch (e) {
                      
                      // If the result isn't JSON, probably a PHP error
                      MT.view.modal.show('error');
                      console.error(data);
                      return;
                    }
                    
                    var i = 0;
                    // Make each result into an instance of the 'post' model
                    $.each(data[that.type], function (i, report) {
                      
                      report.id = report[that.idAttr];
                      report.type = that.singleType;
                      that.add(report);
                      
                      i++;
                    });
                    
                    that.trigger('load:reports');
                    that.offset += that.page;
                    that.loading = false;
                  });
          
          // Just in case we missed an error case
          req.error(function () {
            MT.view.modal.show('error');
          });
        },
        
        // Get all of the comments on the posts
        getComments: function () {
          this.forEach(function (post) {
            post.getComments();
          });
          
          return false;
        }
      }),
      
      
      Comments: Backbone.Collection.extend({
        idAttr: 'comment_id',
        type: 'comments',
        singleType: 'comment',
        model: Models.Post,
        
        initialize: function (id, callback) {
          this.report_id = id;
/*          this.report = MT.model.getCollection('Reports').get(this.report_id); */
          var that = this;
          
          this.on('load:comments', function () {
            MT.view.reports.renderComments(that);
          });
          
          $.ajax({
            type: 'POST',
            url: API_URL + 'user/get/report/comments/',
            data: {
              report_id: that.report_id
            },
            dataType: 'json',
            
            success: function (data) {
              if (data && !data.error) {
                
                $.each(data[that.type], function (i, comment) {
                  comment.id = comment[that.idAttr];
                  comment.type = that.singleType;
                  that.add(comment);
                });
                
                if (typeof callback == 'function') {
                  callback();
                }
                
                that.trigger('load:comments');
              }
            }
          });
        }
      })
    };
    
    return {
      models: {},
      
      // This is where the models & collections come from.
      // It'll only instantiate the model once.
      getModel: function (name) {
        if (this.models[name] == undefined) {
          this.models[name] = new Models[name]();
        }
        
        return this.models[name];
      },
      
      getCollection: function (name) {
        if (this.models[name] == undefined) {
          this.models[name] = new Collections[name];
        }
        
        return this.models[name];
      }
    };
  }();
  
  
  MT.view = function () {
      
    var $container;
       
    // Settings for the two different sizes of spinner the app uses.
    var spinner_opts_large = {
      lines: 9, // The number of lines to draw
      length: 0, // The length of each line
      width: 8, // The line thickness
      radius: 13, // The radius of the inner circle
      rotate: 0, // The rotation offset
      color: '#000', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: true, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: 'auto' // Left position relative to parent in px
    };
    var spinner_opts_small = {
      lines: 8, // The number of lines to draw
      length: 0, // The length of each line
      width: 4, // The line thickness
      radius: 6, // The radius of the inner circle
      rotate: 0, // The rotation offset
      color: '#000', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner_tiny', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: 'auto' // Left position relative to parent in px
    };
    
    // Will return a Spinner object of the proper size, according to the above settings
    // Uses spin.js -- http://fgnass.github.com/spin.js/
    function spinFactory(size) {
      if (size == 'large') {
        return new Spinner(spinner_opts_large);
      } else if (size == 'small') {
        return new Spinner(spinner_opts_small);
      }
    }
    
    // Sticky headers for non-window 'viewports'
    // new Sticky (element, parent, viewport)
    var Sticky = Backbone.View.extend({
      initialize: function (el, parent, viewport) {
        this.el = el;
        this.$el = $(el);
        this.parent = parent;
        this.$parent = parent.$el;
        
        // If the viewport wasn't passed to the constructor, check to see
        // if a default viewport has been specified in an extension of the object
        if (viewport) {
          this.$viewport = viewport;
        } else {
          this.$viewport = $(this.viewport);
        }
        
        this.viewHeight = this.$viewport.height();
        this.zoneTop = this.$viewport.offset().top;
        this.currentState = 'parent-top';
        
        this.eventsToDelegate = {};
        
      },
      
      on: function () {
        this.checkScroll();
        
        var that = this;
        
        // Namespace the scroll event with the cid (backbone object id)
        // of the object, so we can remove it individually later
        this.$viewport.on('scroll.' + this.parent.cid, function () {
          that.checkScroll();
        });
        
        return this;
      },
      
      off: function () {
        this.$viewport.off('scroll.' + this.parent.cid);
        return this;
      },
      
      // Fires on every scroll event. Check if we need to change the 'sticky' status
      // Essentially, defines a "sticky zone" equivalent to the height of the sticky element
      // If the top of the parent passes the top of the zone, the element sticks, if the bottom
      // of the parent passes the bottom of the zone, the element sticks to the bottom of the parent
      // To make it stick to the top, we actually make a copy of it outside the viewport.
      // This may require extra CSS trickery.
      checkScroll: function () {
      
        // Position of the top & bottom of the parent element, top of the sticky element
        var parentTop = this.$parent.offset().top,
          parentBottom = this.$parent.offset().top + this.$parent.height(),
          elTop = this.$el.offset().top;
        
        // Can't set the zone bottom at construction, because often the element doesn't exist on the page yet
        // Still, we only want to set it once
        if (!this.zoneBottom) {
          this.zoneBottom = this.$viewport.offset().top + this.$el.height();
        }
        
        // Check the current state. Possible states are:
        // 'sticky', 'parent-top', 'parent-bottom'
        if (this.currentState == 'parent-top') {
        
          if (parentTop <= this.zoneTop) {
            this.stickify();
            this.currentState = 'sticky';
            return;
          }
        
        } else if (this.currentState == 'sticky') {
        
          if (parentTop >= this.zoneTop) {
            this.unstickify('top');
            this.currentState = 'parent-top';
            return;
          }
          
          if (parentBottom < this.zoneBottom) {
            this.unstickify('bottom');
            this.currentState = 'parent-bottom';
            return;
          }
        
        } else if (this.currentState == 'parent-bottom') {
        
          if (parentBottom >= this.zoneBottom) {
            this.stickify();
            this.currentState = 'sticky';
            return;
        
          }
        }
      },
      
      // Make it actually stick to the top
      stickify: function () {
      
        // If the copy of the element already exists, don't make it again
        if (this.$stickyEl) {
        
          // Show the sticky copy
          this.$stickyEl.show();
          this.stickyView.delegateEvents(this.eventsToDelegate);
          // Hide the original
          this.$el.css('visibility', 'hidden');
        } else {
          
          // Hide all other sticky elements
          this.$viewport.parent().find('.sticky').hide();
          
          // Copy the element
          this.$stickyEl = this.$el.clone();
          this.$viewport.before(this.$stickyEl);
          
          this.stickyView = new Backbone.View({el: this.$stickyEl.get(), model: this.parent.model});
          this.stickyView.events = this.eventsToDelegate;
          
          var that = this;
          this.stickyView.delegateEvents(this.eventsToDelegate);
          this.stickyView.$el.on('click', function (event) {
            if (event.target.tagName == 'A') {
              return;
            }
            
            if (that.parent.extended) {
              that.parent.collapse();
              
              $(this).fadeOut(function () {
                that.unstickify();
              });
            }
          });
          
          // Stick it to the top of the viewport. I actually have no idea what the fuck this.$zoneTop is
          // It really should be this.zonetop but when I change it it breaks. So....
          this.$stickyEl.css({
            position: 'absolute',
            top: this.$zoneTop,
            bottom: 'auto',
            zIndex: 999
          }).addClass('sticky');
          
          this.$el.css('visibility', 'hidden');
        }
      },
      
      unstickify: function (dir) {
        
        // Hide the fake element
        if (this.$stickyEl && !this.$stickyEl.is(':hidden')) {
          this.$stickyEl.hide();
          this.stickyView.undelegateEvents();
        }
        
        // Show the real element
        this.$el.css('visibility', 'visible');
        
        // Restore the original position of the element. (in case it was stuck to the bottom)
        if (dir == 'top' && this.cacheTop !== null) {
          this.$el.css({
            top: this.cacheTop,
            zIndex: this.cacheZ,
            position: this.cachePosition
          });
          
          this.cacheTop = this.cachePosition = this.cacheZ = null;
        }
        
        // Stick it to the bottom of the parent
        if (dir == 'bottom') {
          var newTop = this.$parent.height() - this.$el.height();     
          
          // Only cache the top position if it's different
          // Avoids a situation in which case it'll permanently stick to the bottom
          if (newTop != parseInt(this.$el.css('top'), 10)) {
            this.cacheTop = this.$el.css('top');
            this.cachePosition = this.$el.css('position');
            this.cacheZ = this.$el.css('z-index');
          }
          
          this.$el.css({
            top: newTop,
            zIndex: 9999,
            position: 'relative'
          });
        }
      },
      
      toggleClass: function (className) {
        if (this.stickyView) {
          this.stickyView.$el.toggleClass(className);
        }
      },
      
      removeClass: function (className) {
        if (this.stickyView) {
          this.stickyView.$el.removeClass(className);
        }
      },
      
      goAway: function () {
        var that = this;
        this.stickyView.$el.fadeOut(function () {
          $(this).remove();
          that.off();
          delete that;
        });
        
      }
    });
    
    
    var stickyTitle = Sticky.extend({
      viewport: 'ul.comments'
    });
    
    
    // Modal alert dialog box
    // Use it as: MT.view.modal.show(name, [message, [callback]])
    // Expects a #modal element, and an element for every dialog you expect to call.
    // Dialog should have their name be a class, and contain an option .message element
    // for the 'message' argument.
    var Modal = Backbone.View.extend({
      initialize: function () {
        this.$el = $('#modal');
        this.el = this.$el.get();
      },
      
      show: function (dialog, message, callback) {
      
        if (!this.$('.' + dialog).length) {
          return false;
        }
        
        if (typeof message == 'function') {
          callback = message;
          message = null;
        }
        
        var $dialog = this.$('.' + dialog);
        this.$currentDialog = dialog;
        
        // Save the default text & replace it with the passed message
        if (message && $dialog.find('.message')) {
          this.cachedMessage = $dialog.find('.message').text();
          $dialog.find('.message').text(message);
        }
        
        $dialog.show();
        this.$el.fadeIn();
        
        // Capture the Enter key — no other objects get it!
        MT.controller.keymaster.captureEnter();
        
        // Default button.
        var $def = $dialog.find('.default'),
           that = this;
        
        // There can only be one... default button
        if ($def.length && $def.length > 1) {
          $def = $def[0];
        }
        
        // Button to be called on esc
        // esc defaults to closing the box
        var $esc = $dialog.find('.esc');
        if ($esc.length && $esc.length > 1) {
          $esc = $esc[0];
        }
        
        this.on('enterpress', function () {
          $def.focus();
          $def.trigger('click');
        });
        
        this.on('escpress', function () {
        
          // If no button is set to capture escape, just close the dialog
          if (!$esc.length) {
            this.close(function () {
              $dialog.hide();
              
              if (that.cachedMessage) {
                $dialog.find('.message').text(that.cachedMessage);
                that.cachedMessage = null;
                $dialog.hide();
              }
              
            });
          }
          
          $esc.focus();
          $esc.trigger('click');
          MT.controller.keymaster.stopCapture();
        });
        
        $dialog.find('button').on('click', function (event) {
          event.preventDefault();
          
          // Close the box
          that.close(function() {
          
            // Restore any default message
            if (that.cachedMessage) {
              $dialog.find('.message').text(that.cachedMessage);
              that.cachedMessage = null;
            }
            
            $dialog.hide();
            
          });
          
          if (typeof callback == 'function') {
            callback($(this).text());
          }
          
          // Stop listening for the click (the button does still exist, after all)
          $dialog.find('button').off('click');
          
          return;
        });
      },
      
      close: function (callback) {
        this.$el.fadeOut(function () {
        
          if (typeof callback == 'function') {
            callback();
          }
          
        });
        
        this.off('enterpress');
        this.off('escpress');
      }
    });
    
    // Generic panel 
    var Panel = Backbone.View.extend({
      className: 'panel',
      isOpen: false,
      content: null,
      
      open: function () {
        
        this.$el.css({
          opacity: 0
        })
        .removeClass('closed')
        .animate({
          opacity: 1
        }, 150);
              
        this.isOpen = true;
        
        if (this.onOpen && typeof this.onOpen == 'function') {
          this.onOpen();
        }
        
        return this;
      },
      
      close: function () {
        
        this.$el.animate({
          opacity: 0
        }, 150, function () {
          $(this).addClass('closed');
        });
              
        this.isOpen = false;
        
        return this;
      },
      
      replace: function () {
        
      }
      
    });
    
    // Generic 'posts' panel...
    var Posts = Panel.extend({
      initialize: function () {
        this.postsLoaded = false;
      },
      onLoadCB: [],
      
      // Set up a callback for when the posts load, if they're already loaded, execute it now
      // Also used to call the callback.
      onLoad: function(callback) {
        
        if (typeof callback == 'function') {
          this.onLoadCB.push(callback);
        }
      
        if (!this.onLoadCB.length) {
          return;
        }
        
        if (this.loadedReports != this.collection.length) {
          return;
        }
        
        var loaded = true;
        
        
        if (!this.postsLoaded) {
          // Make sure all the posts are actually loaded
          _.forEach(this.reports, function (view) {
            if (!view.ready) {
              loaded = false;
            }
          });
        }
        
        if (!loaded) {
          return;
        }
        
        this.postsLoaded = true;
        
        _.forEach(this.onLoadCB, function (cb) {
          cb();
        });
        
        this.onLoadCB = [];
      },
            
      onOpen: function () {
        
        // Only execute the first time it's opened
        if (!this.hasBeenOpened) {
          this.hasBeenOpened = true;
        
          // Make a spinner
          this.spinner = spinFactory('large').spin();
          this.$('.main_report').append(this.spinner.el);
          
          // Load the models
          this.collection = MT.model.getCollection('Reports');  
          
          // Get the template from the DOM
          this.postTemp = $(this.$('#post_template').html())[0];
          
          this.$viewport = this.$ul = this.$('ul');
          
          this.$viewport.on('scroll', function () {
            var scroll = $(this).scrollTop();
            var height = this.scrollHeight;
            var viewHeight = $(this).height();
            
            if (scroll + viewHeight >= (height - 6)) {
              MT.model.getCollection('Reports').loadPosts();
            }
          });
          
        }
        
      },
      
      
      renderPosts: function () {
      
        if (!this.collection || !this.open) {
          return;
        }
        
        // Get rid of the spinner
        var that = this;
        this.spinner && $(this.spinner.el).fadeOut(function () {
          that.spinner.stop();
        });
        
        // Make views
        
        if (!this.reports) {
          this.reports = {};
        }
        
        this.collection.forEach(function (report, i) {
          
          that.loadedReports = i + 1;
          
          console.log( that.reports[parseInt(report.id, 10)]);
          
          if (that.reports[parseInt(report.id, 10)]) {
            return;
          }
          
          // Array indexed by report ID
          that.reports[report.id] = new Post(
          
            // The post template
            that.template,
            
            // The model
            report,
            
            // A reference to the Posts panel
            that
          );
          
          report.view = that.reports[report.id];
        });
        
        // Do the comments thing
        this.collection.getComments();
        
      },
      
      renderComments: function (comments) {
        this.reports[comments.report_id].addCommentBlock(comments);
      },
      
      events: {
        
        // Nav bar at the top
        'click nav p' : function (event) {
          this.$('nav p.active').removeClass('active');
          $(event.currentTarget).addClass('active');
          var act = $(event.currentTarget).text();
          
          switch (act) {
            case 'All Reports':
            case 'All Comments':
              this.showAll();
            break;
            
            case 'Pending':
              this.showPending();
            break;
            
            case 'Featured':
              this.showFeatured();
            break;
          }
          
          this.scrollToTop();
        }
      },
      
      // Find the post following a given ID
      findNextExisting: function (id) {
        var previous = null;
        var found = false;
        
        this.posts.forEach(function (post) {
          if (!found) {
            if (post.id == id) {
              found = true;
            } else if (post.exists) {
              previous = post;
            }
          }
        });
        
        return previous ? previous : false;
      },
      
      // Scroll to a given report
      // Accepts an ID or JQuery element
      scrollToReport: function (which) {
        
        // Wait until the posts load to scroll to the report
        var that = this;
        this.onLoad(function () {
          if (typeof which == 'number') {
            $report = that.reports[which].$el;
          } else {
            $report = which;
          }
                  
          // The amount by which the viewport needs to scroll to get to the post
          var offset = $report.offset().top - that.$viewport.offset().top + that.$viewport.scrollTop();
          
          that.$ul.animate({
            scrollTop: offset
          });
        });
      },
      
      scrollToTop: function () {
        this.$ul.animate({
          scrollTop: -this.$ul.scrollTop()
        });
      },
      
      // Show all the reports, duh
      showAll: function () {
      
        _.forEach(this.reports, function (report) {
          report.hideIf();
          report.collapseIfExtended();
        });
        
        this.currentTab = 'all';
      },
      
      // Show only reports in the pending state
      showPending: function () {
      
        _.forEach(this.reports, function (report) {
          report.hideIf('approved');
        });
        
        this.currentTab = 'pending';
                
        var that = this;
        window.setTimeout(function () {
          
          var height = that.$viewport[0].scrollHeight;
          var viewHeight = that.$viewport.height();
          
          console.log(height, viewHeight);
          
          if (height <= viewHeight) {
            MT.model.getCollection('Reports').loadPosts();
          }
        }, 1000);
      },
      
      // Show only reports in the featured state
      showFeatured: function () {
      
        _.forEach(this.reports, function (report) {
          report.hideIf('featured', 'not');
          report.collapseIfExtended();
        });
        
        this.currentTab = 'featured';
        
        var that = this;
        window.setTimeout(function () {
          
          var height = that.$viewport.scrollHeight;
          var viewHeight = that.$viewport.height();
          
          console.log(height, viewHeight);
          
          if (height <= viewHeight) {
            MT.model.getCollection('Reports').loadPosts();
          }
        }, 1000);
        
      }
    });
    
    
    // Generic Post view
    var Post = Backbone.View.extend({
      
      // Do posts collapse & expand?
      collapsable: false,
      
      ready: false,
      
      // Should the comments be retracted or expanded by default?
      showCommentsByDefault: false,
      
      initialize: function (element, model, parent, $template) {
        this.model = model;
        this.parent = parent;
        this.id = this.model.id;
        this.$viewport = parent.$viewport;
        
        this.$el = $(element);
        this.el = this.$el.get();
        
        // Sometimes the views like to get philosophical
        this.exists = true;
        
        var that = this;
        
        // Add the title to the top, if we're in the post template
        if (this.$el.find('.title').length) {
          this.$title = this.$('.title');
          this.$title.find('h1').text(model.get('title'));
          
          this.sticky = new stickyTitle (this.$title, this, this.$viewport);
          this.sticky.model = this.model;
          _.extend(this.sticky.eventsToDelegate, this.events);
          that = this;
        }
        
        this.$el.addClass(this.model.get('status'));
        this.$title && this.$title.addClass(this.model.get('status'));
        
        if (this.model.get('featured') == 1) {
          this.$el.addClass('featured');
          this.$title && this.$title.addClass('featured');
        }
        
        var text = this.model.get(this.parent.contentName);
        var paragraphs = text.split('\n');
        
        that = this;
        var $full = this.$('.full');
        this.extended = this.showCommentsByDefault;
        
        // Split into paragraphs by line breaks
        // Revise to ignore empty lines
        _.forEach(paragraphs, function (p) {
          var $pEl = $('<p />');
          $pEl.text(p);
          $full.append($pEl);
        });
        
        // Add any pictures or video 
        if (this.model.getMedia()) {
          var media = this.model.getMedia();
          var $media;
          
          if (media && media.image) {
            $media = $('<img />');
            $media.attr('src', media.image);
          }
          
          if (media && media.video) {
            $media = $('<video />');
            $media.attr('src', media.video)
                .attr('preload', 'none');
            
            MT.controller.videoQueue($media);
            
            // We're using HTML 5 video. This is as un-insulting to IE users as I can be
            // I wanted to say 'We suggest you join the 21st century' but that seemed mean-spirited 
            $media.text('You seem to be using a browser that does not support HTML 5 video.' +
                  'We suggest you download a modern browser, such as'+
                  '<a href="http://chrome.google.com">Google Chrome.</a>');
                  
            var orientation = this.model.get('videoorientation');
            orientation && $media.addClass(orientation);
          }
          
          $media && $media.addClass('media');
          $full.append($('<div />').addClass('media_wrapper').append($media));
          
          if (media && media.video) {
            var $wrapper = $full.find('.media_wrapper');
            var $controls = $('<div />').addClass('video_controls');
            // $controls.hide();
            
            $wrapper.append($controls);
            
            var $button = $('<img />').attr('src', 'images/play.png');
            $controls.append($button);
            
            $wrapper.on('mouseover', function () {
              if ($controls.is(':hidden')) {
                $controls.fadeIn();
              }
            });
            
            $wrapper.on('mouseleave', function (e) {
              if ($media.hasClass('playing')) {
                $controls.fadeOut();
              }
            });
            
            $controls.on('click', function () {
              if (!$media.hasClass('playing')) {
                $media.get(0).play();
                $media.addClass('playing');
                $button.attr('src', 'images/pause.png');
                $controls.fadeOut();
              } else {
                $media.get(0).pause();
                $media.removeClass('playing');
                $button.attr('src', 'images/play.png');
              }
            });
          }
          
          $media && $media.on('load', function () {
            that.parent.onLoad && that.parent.onLoad();
          });
          
        }
        
        // Add the meta data
        var metaString = 'Posted by ' +
                    this.model.get('firstname') + ' ' + 
                    this.model.get('lastname') + ' at ' +
                    this.model.get('create_time');
        
        this.$('.meta').text(metaString);
                
        // Make it fade in
        this.$el.hide();
        parent.$ul.append(this.$el);
        
        if (parent.currentTab == 'pending' && this.$el.hasClass('approved')) {
          this.hideIfApproved = true;
        } else if (parent.currentTab == 'featured' && !this.$el.hasClass('featured')) {
          // do nothing i guess
        } else {
          this.$el.fadeIn();
        }
        
        if ($media) {
          // Center the media
          var mediaWidth = $media.width();
          
          if (mediaWidth > 0) {
            $media.css({
              width: mediaWidth
            });
          }
        }
        
        this.ready = true;
        
      },
      
      // When 'Featured', 'Approved' etc are modified, update the view
      updateStatus: function (status, model) {
        switch (status) {
          case 'approved':
            this.$el.toggleClass('approved');
            this.$title && this.$title.toggleClass('approved');
            this.sticky && this.sticky.toggleClass('approved');
            
            if (this.$el.hasClass('approved') && this.$el.hasClass('denied')) {
              this.$el.removeClass('denied');
              this.$title && this.$title.removeClass('denied');
              this.sticky && this.sticky.removeClass('denied');
            }
            
            // If this isn't allowed to be shown right now
            if (this.$el.hasClass('approved') && this.hideIfApproved) {
              this.slideOut();
            }
            
            this.parent.trigger('nextPost', this.id);
          break;
          
          case 'denied':
            this.$el.toggleClass('denied');
            this.sticky && this.sticky.toggleClass('denied');
            this.$title && this.$title.toggleClass('denied');
            
            if (this.$el.hasClass('approved') && this.$el.hasClass('denied')) {
              this.$el.removeClass('approved');
              this.$title && this.$title.removeClass('approved');
              this.sticky && this.sticky.removeClass('approved');
            }
          break;
          
          case 'featured':
            this.$el.toggleClass('featured');
            this.$title && this.$title.toggleClass('featured');
            this.sticky && this.sticky.toggleClass('featured');
          break;
          
          case 'selected':
            
            this.$el.toggleClass('selected');
            this.$title && this.$title.toggleClass('selected');
            this.sticky && this.sticky.toggleClass('selected');

            if (this.$el.hasClass('selected')) {
              MT.view['export'].addReport(model);
            } else {
              MT.view['export'].removeReport(model);
            }
            
          break;
          
          default:
          break;
        }
      },
      
      addCommentBlock: function (data) {
        this.comments = new this.commentContainer(this.model.id, data, this);
        this.$title.addClass('has_comments');
        if (this.extended) {
          this.sticky.on();
        }
      },
      
      
      // Sub-UL to hold comments
      commentContainer: Backbone.View.extend({
        
        initialize: function (report, comments, parent) {
          this.contentName = 'text';
          this.parent = parent;
          this.$el = this.$ul = $('<ul />');
          this.$el.addClass('comment_wrapper');
          this.$el.attr('id', report.id);
          this.template = $('#comment_template').html();
          this.posts = [];
          var that = this;
          
          comments.forEach(function (item) {
          
            // For whatever reason, we get some comments without text or anything
            if (!item.get('text')) {
              return;
            } 
            
            that.posts[item.id] = new Post(
                that.template,
                item,
                that
            );
            
            that.posts[item.id].$el.hide();
            
            that.parent.showCommentsByDefault && that.posts[item.id].slideDown();
            
            item.view = that.posts[item.id];
          });
          
          $(this.parent.$el).append(this.$el);
        }
      }),
      
      
      // Hide any element with the given class, or not, as the case may be
      hideIf: function (className, not) {
        var hideEl = (not && className && !this.$el.hasClass(className)) ||
                 (!not && className && this.$el.hasClass(className));
                 
        if (!className) {
          hideEl = false;
          this.hideIfApproved = false;
        }
        
        if (hideEl) {
          var that = this;
          
          this.$el.animate({opacity: 0}, function () {
            that.slideUp(function () {
              that.$el.css('opacity', 1);
              that.collapseIfExtended();
            });
          });
        
        } else {
          
          if (this.$el.is(':hidden')) {
            this.slideDown();
          }
          
        }
        
        // Make sure that if the state changes, it hides appropriately
        if (className == 'approved') {
          this.hideIfApproved = true;
        } else {
          this.hideIfApproved = false;
        }
        
        if (hideEl) {
          return true;
        } else {
          return false;
        }
      },
      
      // Slide left from the screen
      slideOut: function (callback) {
      
        // Save the original position
        var cache = this.$el.css('margin-left');
        
        this.$el.find('p').css({
          maxWidth: this.$el.css('width')
        });
        
        this.$el.find('.button_set').fadeOut();
        
        var that = this;
        this.$el.animate({
          marginLeft: (this.$el.width() * -1  ) -
                  (parseInt(this.$el.css('padding-left'), 10) * 2)
        }, 500,
        
        // After the animation
        function () {
          that.slideUp(function () {
            
            // Restore width & button set
            that.$el.css('margin-left', cache);
            that.$('.button_set').show();
            
            // It should be collapsed next time it apears
            if (that.extended) {
              that.collapse();
            }
            
            if (callback && typeof callback == 'function') {
              callback();
            }
          });
        });
      },
      
      // Slide up animation. The built-in JQuery one was glitchy
      slideUp: function (callback) {
        
        // Save the original position
        var cache = this.$el.css('margin-bottom');
        
        if (!this.$('.button_set').is(':hidden')) {
          this.$('.button_set').fadeOut();
        }
        
        this.$el.animate(
          {
            marginBottom: (this.$el.height() * -1)
                      - parseInt(this.$el.css('padding-bottom'), 10)
                      - parseInt(this.$el.css('padding-top'), 10)
          }, 500,
          
          // After the animation
          function () {
          
            $(this).hide();
            $(this).css('margin-bottom', cache);
            
            if ($(this).find('.button_set').is(':hidden')) {
              $(this).find('.button_set').show();
            }
            
            if (callback && typeof callback == 'function') {
              callback();
            }
          }
        );
      },
      
      slideDown: function (callback) {
        var cache = this.$el.css('margin-bottom');
        
        var cacheBorder = {
          top: this.$el.css('borderTopWidth'),
          bottom: this.$el.css('borderBottomWidth')
        };
                
        this.$el.css({
          borderBottomWidth: 0,
          borderTopWidth: 0,
          opacity: 0
        });
        
        this.$el.css('margin-bottom', (this.$el.height() * -1)
                            - parseInt(this.$el.css('padding-bottom'), 10)
                            - parseInt(this.$el.css('padding-top'), 10));
                            
        this.$el.show();
        
        this.$el.animate(
          {
            marginBottom: cache,
            opacity: 1
          }, 
          
          // After the animation
          function () {
            
            $(this).css({
              borderBottomWidth: cacheBorder.bottom,
              borderTopWidth: cacheBorder.top
            });
            
            if (typeof callback == 'function') {
              callback();
            }
          }
        );
      },
      
      
      // Show/hide the comments
      expand: function () {
        if (!this.comments) {
          return;
        }
        
        // If comments land below viewport, scroll down
        var that = this;
        _.forEach(this.comments.posts, function (comment) {
          comment.slideDown();
          
          var bottomOfComment = comment.$el.offset().top + comment.$el.height();
          var bottomOfViewport = that.$viewport.offset().top + that.$viewport.height();
          
          var diff = bottomOfComment - bottomOfViewport;
          
          if (diff > 0) {
            diff = that.$viewport.scrollTop() + diff;
            
            that.$viewport.animate({
              scrollTop: diff
            });
          }
          
        });
        
        this.$title.find('small').text('Hide Comments');
        
        this.sticky.on();
        
        this.extended = true;
      },
      
      collapse: function () {
        if (!this.comments) {
          return;
        }
        
        _.forEach(this.comments.posts, function (comment) {
          comment.slideUp();
        });
        
        this.$title.find('small').text('Show Comments');
        
        this.sticky.off();
        
        this.extended = false;
      },
      
      
      // Pretty self explanatory. On all counts, really.
      collapseIfExtended: function () {
        if (this.extended) {
          this.collapse();
        }
      },
      
      remove: function () {
        var that = this;
        this.slideOut(function () {
          that.$el.remove();
          that.exists = false;
        });
        
        if (this.extended) {
          this.sticky.goAway();
        }
      },
          
      events: {
        'click .title' : function (event) {
          if(event.target.tagName == 'A') {
            return;
          }
          
          if (!this.extended) {
            this.expand();
          } else if (this.extended) {
            this.collapse();
          }
        },
        
        'click .approve_button' : function () {
          if (!this.$el.hasClass('approved')) {
            this.model.setApproved(true);
          } else {
            this.model.setApproved(false);
          }
        },
        
        'click .feature_button' : function () {
          if (!this.$el.hasClass('featured')) {
            this.model.setFeatured(true);
          } else {
            this.model.setFeatured(false);
          }
        },
        
        'click .reject_button' : function (event) {
          event.stopPropagation();
          var that = this;
          
          // Don't confirm delete if the button was option-clicked
          if (MT.controller.keymaster.isPressed('option')) {
            that.model.destroy();
            return;
          }
          
          // Otherwise pop up a confirmation dialog
          MT.view.modal.show('delete', function (response) {
            if (response == 'OK') {
              that.model.destroy();
            }
          });
        },
        
        'click .select_for_download_button' : function () {
          if (!this.$el.hasClass('selected')) {
            this.model.setSelected(true);
          } else {
            this.model.setSelected(false);
          }
        },
      }
    });
    
    
    // Specific views, extended from the generic classes above.
    var views = {
      Reports: Posts.extend({
        initialize: function () {
          this.id = 'reports';
          this.$el = $('#reports');
          this.el = this.$el.get();
          this.modelName = 'Reports';
          this.content = this.$('ul');
          this.renderOn = 'load';
          this.contentName = 'content';
          this.singular = 'report';
          this.template = $('#report_template').html();
        }
      }),
      
      Twitter: Panel.extend({
        
        // When the user submits a new hashtag
        // But after it's been recieved by the server.
        // Includes a totally sweet animation.
        updateTag: function () {
        
          // Mini-spinner
          if (this.$('.spinner_tiny').length) {
            this.$('.spinner_tiny').remove();
          }
          
          var newTag = MT.model.getModel('Twitter').get('tag');
          
          // If this is the first load, the field will just be '#'
          if (this.$('#new_hashtag').val() != '#') {
            
            // Reset the form field
            this.$('#new_hashtag').val('#');
            
            // Add the class that'll make the CSS transition go
            $('#hashtag_effect').text(newTag).addClass('up');
            
            // Fade out the current tag
            $('#current_hashtag').animate({opacity: 0});
            
            // When the CSS transition is done, replace the effect element with the real element
            window.setTimeout(function () {
              this.$('#current_hashtag').text(newTag).css({opacity: 1});
              
              // And reset the effect element
              $('#hashtag_effect').html('').removeClass('up');
            }, 1000);
            
          } else {
            this.$('#current_hashtag').text(newTag);
            this.$('#current_hashtag').slideDown();
          }
        },
        
        updateTweets: function () {
          var that = this;
          
          var makeTweets = function () {
              
            // Reset the container
            this.$('#tweets').html('').hide();
            
            var tweets = MT.model.getModel('Twitter').get('tweets');
            
            if (tweets && tweets.length) {
            
              // Make an li elemment for each tweet
              for (var i = 0; i < tweets.length; i++) {
                var tweet = tweets[i],
                   tweetEl = $('<li />');
                
                tweetEl.html('<strong>' + tweet.from_user_name + 
                         ':</strong> ' + tweet.text);
                this.$('#tweets').append(tweetEl);
              }
            } else {
              this.$('#tweets').append('<li>Could not load tweets</li>');
            }
            
            this.$('#tweets').fadeIn();
          
          };
          
          if (this.$('.spinner').length) {
            this.$('.spinner').fadeOut(function () {
              makeTweets();
            });
          } else {
            makeTweets();
          }
        },
        
        events: {
        
          // Submit a new hashtag button
          'click #twitter_submit' : function (event) {
            event.preventDefault();
            
            var form = this.$('#new_hashtag'),
               newHashtag = form.val();
            
            // Validate the hashtag
            // Add to this
            if (newHashtag == '#' || newHashtag == '') {
            
              MT.view.modal.show('error', 'The hashtag you entered was empty.', function () {
                form.focus();
              });
                            
              return false;
              
            } else if (/[\s]/.exec(newHashtag)) {
            
              MT.view.modal.show('error', 'Twitter hashtags can\'t contain a space.', function () {
                form.focus();
              });
              
              return false;
            }
            
            // Make a tiny spinner
            var spinner = spinFactory('small').spin();
            this.$('#twitter_form').append(spinner.el);
            
            // Go tell the model
            MT.model.getModel('Twitter').set({tag: newHashtag});
          },
          
          // Make sure the '#' stays in the form field at all times
          // Improve this
          'keyup #new_hashtag' : function () {
            var prev_value = '#',
               field;
            
            return function (event) {
              if (!field) {
                field = this.$('#new_hashtag');
              }
              
              // The timeout is because the event fires before the value of the text input updates
              window.setTimeout(function () {
                
                if (field.val() == '') {
                  field.val('#');
                } else if (field.val().charAt(0) !== '#') {
                  event.preventDefault();
                  event.stopPropagation();
                  
                  field.val(prev_value);
                  
                  return false;
                } else {
                  prev_value = field.val();
                }
              }, 0.2);
            };
          }()
        },
        
        
        initialize: function () {
          this.id = 'twitter';
          this.el = $('#twitter').get();
          this.$el = $('#twitter');
          this.$('#current_hashtag').hide();
          this.content = this.$('#tweets');
        },
        
        onOpen: function () {
        
          if (this.$('#current_hashtag').text() == ''
             && !this.$('.spinner').length) {
             
            this.spinner = spinFactory('large').spin();
            this.$('#tweet_wrapper').append(this.spinner.el);
          }
          
          this.updateTag();
        }
      }),
      
      
      Export: Panel.extend({
        initialize: function () {
          this.id = 'export';
          this.el = $('#export').get();
          this.$el = $('#export');
          this.$reportList = $('ul#selected-reports');
          this.$reportForm = $('form#report_ids');
          this.$idInput = $('form#report_ids > input[name="report_id"]');
          this.report_ids = [];
          
          this.clearReports();
        },
        
        addReport: function(model) {
          if (this.report_ids.length == 0) this.clearReports();
          this.report_ids.push(model.get('report_id'));
          this.$reportList.append('<li id="report_id-' + model.get('report_id') + '">' + model.get('title') + '</li>');
          // place the ids in the hidden input as a comma-separated string
          this.$idInput.val(this.report_ids.toString());
        },
        
        removeReport: function(model) {
          this.report_ids = _.without(this.report_ids, model.get('report_id'));
          this.$reportList.find('li#report_id-' + model.get('report_id')).remove();
          // place the ids in the hidden input as a comma-separated string
          this.$idInput.val(this.report_ids.toString());
          
          if (this.report_ids.length == 0) this.clearReports();
        },
        
        clearReports: function() {
          if (this.$reportList.hasClass('null')) {
            this.$reportList.removeClass('null');
            this.$reportList.empty();
          } else {
            this.$reportList.addClass('null');
            this.$reportList.append('<li>none</li>');
          }
          
        },
        
        events: {
          'click #csv_user' : function (event) {
            MT.model.getModel('Export').downloadCSV('users');
          },
          
          'click #csv_reports' : function (event) {
            MT.model.getModel('Export').downloadCSV('reports');
          },
          
          'click #csv_report' : function (event) {
            event.preventDefault();
            // only download if there's reports selected
            if (this.report_ids.length) {
              this.$reportForm.submit();
            }
            
          }
        }
      }),
      
      Sponsored: Panel.extend({
        initialize: function () {
          this.id = 'message';
          this.$el = $('#message');
          this.el = this.$el.get();
        },
        
        onOpen: function () {
          this.model = MT.model.getModel('Sponsored');
          
          var today = new Date(),
             month = (today.getMonth() + 1).toString(),
             day = today.getDate().toString(),
             year = today.getFullYear().toString();
          
          if (day.length == 1) {
            day = '0' + day;
          }
          
          if (month.length == 1) {
            month = '0' + month;
          }
          
          this.$('#datepicker').attr('data-date', month + '/' + day + '/' + year);
          this.$('#datepicker input').val(month + '/' + day + '/' + year);
          
          this.$('#datepicker').datepicker({
            'format': 'mm/dd/yyyy'
          });
          
          var hrs = today.getHours();
          var mins = today.getMinutes();
          
          if (hrs > 12) {
            hrs = hrs - 12;
            this.$('#ampm').val('pm');
          }
          
          if (mins < 10) {
            mins = '0' + mins;
          }
          
          this.$('#time_hours').val(hrs);
          this.$('#time_minutes').val(mins);
          
          var maxChar = this.$('#sponsored_message').attr('data-max');
          this.$('#char_remaining').text('(' + maxChar + ' characters remaining)');
          
        },
        
        insertTowns: function () {
          var towns = this.model.get('towns');
          var $select = this.$('#town_select');
          this.$select = $select;
          this.townsByID = [];
          var that = this;
          _.forEach (towns, function (town) {
            var $li = $('<li />');
            $li.text(town.town_name);
            $li.attr('data-townID', town.town_id);
            $li.hide();
            
            $li.hover(function () {
              that.selectTown($(this));
            });
            
            $li.click(function () {
              that.$('#town_text').val(town.town_name);
              that.$('#town_text').attr('data-townID', $(this).attr('data-townID'));
              that.doAutoComplete();
            });
            
            $('#town_auto').append($li);
            
            that.townsByID[town.town_id] = town.town_name;
          });
          this.$('#town_auto').hide();
          this.on('up', function () {
            this.selectTown('next');
          });
          
          this.on('down', function () {
            this.selectTown('prev');
          });
          
          this.on('enterpress', function () {
            this.current && this.current.trigger('click');
          });
          
          this.on('escpress', function () {
            if (this.hasComplete) {
              this.$('#town_auto').find('li').each(function (i, o) {
                $(o).hide();
              });
              MT.controller.keymaster.stopCapture();
            }
          });
          
        },
        
        doAutoComplete: function () {
          var value = this.$('#town_text').val();
          var showing = 0;
          var that = this;
          this.$('#town_auto').find('li').each(function (i, li) {
            var $li = $(li),
                town = $li.text();
               
            if (value && town.toLowerCase().indexOf(value.toLowerCase()) == 0 &&
               town.toLowerCase() !== value.toLowerCase()) {
              $li.show();
              if (that.$('#town_auto').is(':hidden')) {
                that.$('#town_auto').show();
              }
              showing++;
            } else {
              if (town.toLowerCase() == value.toLowerCase()) {
                that.$('#town_text').attr('data-townID', $li.attr('data-townID'));
              }
              
              $li.hide();
            }
            
          });
                    
          if (this.$('#town_text').val() != this.townsByID[this.$('#town_text').attr('data-townID')]) {
            this.$('#town_text').attr('data-townID') == '';
          }
          
          if (showing > 0) {
            MT.controller.keymaster.captureEnter();
            this.hasComplete = true;
          } else {
            MT.controller.keymaster.stopCapture();
            this.hasComplete = false;
            this.$('#town_auto').hide();
          }
          
        },
        
        selectTown: function (which) {
          var towns = this.$('#town_auto').find('li');
          
          var visibleTowns = [];
          
          var selectedTown;
          var b = 0;
          towns.each(function (i, li) {
            var $li = $(li);
            
            if (!$li.is(':hidden')) {
              visibleTowns.push($li);
              
              if ($li.hasClass('selected')) {
                selectedTown = b;
              }
              
              b++;
            }
          });
          
          if (!visibleTowns) {
            return;
          }
          
          $(visibleTowns[selectedTown]).removeClass('selected');
          if (typeof which == 'string') {
            if (selectedTown || selectedTown === 0) {
              var newSelect;
              if (which == 'next') {
                newSelect = selectedTown - 1;
              } else {
                newSelect = selectedTown + 1;
              }
              
              if (newSelect > visibleTowns.length - 1) {
                return;
              }
              
              
              if (newSelect < 0) {
                return;
              }
              
            } else if (which == 'prev') {
              newSelect = 0;
            }
            
            $(visibleTowns[newSelect]).addClass('selected');
            this.current = $(visibleTowns[newSelect]);
            
          } else if (which.is) {
            which.addClass('selected');
            this.current = which;
          }
          
        },
        
        events: {
          'click #sponsored_go': function (event) {
            event.preventDefault();
            
            if (this.$('#sponsored_message').val() == '') {
              MT.view.modal.show('error', 'You have to enter a message!');
              return;
            }
            
            if (this.$('#time_hours').val() == '' || this.$('#time_minutes').val() == '') {
              MT.view.modal.show('error', 'You have to enter a time!');
              return;
            }
            
            if (/[^0-9]/.exec(this.$('#time_hours').val() + this.$('#time_minutes').val())) {
              MT.view.modal.show('error', 'The time you entered is not valid.');
              return;
            }
            
            if (!this.$('#town_text').attr('data-townID')) {
              MT.view.modal.show('error', 'You forgot to enter a town, or the town you entered is not valid.');
              return;
            }
            
            var hrs;
            if (this.$('#ampm').val() == 'pm') {
              hrs = parseInt(this.$('#time_hours').val(), 10) + 12;
            } else {
              hrs = parseInt(this.$('#time_hours').val(), 10);
            }
            
            var mins = parseInt(this.$('#time_minutes').val(), 10);
            
            var date = new Date(this.$('#datepicker input').val());
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            
            console.log(date_string);
            
            var townID = this.$('#town_text').attr('data-townID');
            
            this.model.set({
              message: this.$('#sponsored_message').val(),
              set_time: date_string,
              town_id: townID,
              url: this.$('#sponsored_url').val()
            });           
          },
          
          'keyup #town_text': function (event) {
            if (event.which == 27) {
              return;
            }
            
            this.doAutoComplete();
          },
          
          'keyup #sponsored_message': function () {
            var textLength = this.$('#sponsored_message').val().length;
            var maxChar = parseInt(this.$('#sponsored_message').attr('data-max'), 10);
            var charRemaining =  maxChar - textLength;
            if (charRemaining < 0) {
              this.$('#sponsored_message').val(this.lastValidMessage);
            } else {
              this.$('#char_remaining').text('(' + charRemaining + ' characters remaining)');
              this.lastValidMessage = this.$('#sponsored_message').val();
            }
            
          },
          
          'keyup #time_hours': function (event) {
          
            var $field = $('#time_hours');
            
            
            if (/[^0-9]/.exec($field.val())) {
              if (this.lastValidHrs) {
                $field.val(this.lastValidHrs);
              } else {
                $field.val('');
              }
            }
            
            
            if (parseInt($field.val(), 10) > 12) {
              $field.val(12);
            }
            
            if (event.which < 48 || event.which > 57) {
              return;
            }
            
            if (parseInt($field.val(), 10) > 1) {
              this.$('#time_minutes').focus();
            }
            
            if ($field.val() !== '') {
              this.lastValidHrs = $field.val();
            }
          },
          
          'keyup #time_minutes' : function (event) {
            var $field = $('#time_minutes');
          
            if (/[^0-9]/.exec($field.val())) {
              if (this.lastValidHrs) {
                $field.val(this.lastValidMins);
              } else {
                $field.val('');
              }
            }
            
            
            if (parseInt($field.val(), 10) > 59) {
              $field.val(59);
            }
            
            if (event.which < 48 || event.which > 57) {
              return;
            }
            
            if (parseInt($field.val(), 10) > 5) {
              this.$('#ampm').focus();
            }
            
            if ($field.val() !== '') {
              this.lastValidMins = $field.val();
            }
          },
          
          'blur #time_minutes' : function () {
          
            var $field = this.$('#time_minutes');
            if ($field.val().length == 1) {
              $field.val('0' + $field.val());
              this.lastValidMins = $field.val();
            }
            
          },
          'change #all_towns' : function () {
            if (this.$('#all_towns').attr('checked')) {
              this.$('#town_text')
                .val('(all towns)')
                .attr('disabled', 'disabled')
                .addClass('alltowns')
                .attr('data-townid', 'all');
            } else {
              this.$('#town_text')
                .removeAttr('disabled')
                .val('')
                .removeClass('alltowns')
                .attr('data-townid', '');
            }
          }
        }
      })
    };
      
    return {
      createViews: function () {
        $container = $('#container');
        MT.view = _.extend(this, {
          login: new Panel ({
            id: 'login'
          }),
          
          modal: new Modal(),
          
          twitter: new views.Twitter(),
          'export': new views.Export(),
          reports: new views.Reports(),
          sponsored: new views.Sponsored()
        });
      }
    };
  }();
  
  
  // Very small controller. More of a utilities object really.
  // Contains a router and a 'keymaster' for dealing with keyboard events
  MT.controller = function () {
    var currentView;
    var videoQ = [];
    var videoLoading;
    var videoCount = 0;
    
    var loadVideo = function () {
      if (videoQ.length) {
        if (videoCount > 9) {
          return;
        }
        videoLoading = true;
        var $v = videoQ.shift();
        
        console.log('loading...');
        $v.attr('preload', 'metadata')
          .on('loadedmetadata', function () {
          $(this).attr('preload', 'none');
          console.log('...loaded');
          window.setTimeout(loadVideo, 500);
        });
        
        videoCount ++;
      } else {
        videoLoading = false;
        console.log('done loading videos');
      }
    };
    
    var closeCurrentView = function () {
      if (currentView) {
        MT.view[currentView].close();
      }
    };
    
    var Router = Backbone.Router.extend({
      routes: {
        'login': 'login',
        'moderate': 'reports',
        'twitter': 'twitter',
        'export': 'export',
        'report/:id': 'reports',
        'sponsored': 'sponsored',
        '': 'reports'
      },
      
      login: function () {
        currentView = 'login';
      },
      
      reports: function (id) {
        if (currentView !== 'reports') {
          closeCurrentView();
          MT.view.reports.open();
          currentView = 'reports';
        }
        
        id && MT.view.reports.scrollToReport(parseInt(id, 10));
      },
            
      comments: function () {
        closeCurrentView();
        MT.view.comments.open();
        currentView = 'comments';
      },
      
      twitter: function () {
        closeCurrentView();
        MT.view.twitter.open();
        currentView = 'twitter';
      },
      
      'export': function () {
        closeCurrentView();
        MT.view['export'].open();
        currentView = 'export';
      },
      
      sponsored: function () {
        closeCurrentView();
        MT.view.sponsored.open();
        currentView = 'sponsored';
      }
    });
        
    return {
      initialize: function () {
        MT.view.createViews();
        var router = new Router();
        Backbone.history.start();
      },
      
      videoQueue: function ($v) {
        videoQ.push($v);
        
        if (!videoLoading) {
          loadVideo();
        }
      },
      
      keymaster: function () {
        var pressedKey = null,
           keyView = false,
           captureEnter = false;
        
        // Keyboard event listener
        $(document).on('keydown', function (event) {
        
          // $MT.controller.keymaster.keyView() in the console
          // For debugging. Or just enable it above.
          if (keyView) {
            console.log(event.which);
          }
          
          switch (event.which) {
            case 13 : // enter
              
              if (captureEnter) {
                event.stopPropagation();
                event.preventDefault();
                captureEnter = false;
              }
            
              MT.view.modal.trigger('enterpress');
              MT.view.sponsored.trigger('enterpress');
              
            break;
            
            case 18: // option
              pressedKey = 'option';
            break;
            
            case 27: // escape
              event.preventDefault();
              MT.view.modal.trigger('escpress');
              MT.view.sponsored.trigger('escpress');
            break;
            
            case 38:
              MT.view.sponsored.trigger('up');
            break;
            
            case 40: 
              MT.view.sponsored.trigger('down');
            break;
          }
        });
        
        $(document).on('keyup', function (event) {
          pressedKey = null;
        });
        
        return {
          isPressed: function (key) {
            return pressedKey == key;
          },
          
          keyView: function () {
            if (keyView) {
              keyView = false;
              console.log('keyView off');
            } else {
              keyView = true;
              console.log('keyView on');
            }
          },
          
          captureEnter: function () {
            captureEnter = true;
          },
          
          stopCapture: function () {
            captureEnter = false;
          }
        };
      }()
    };
  }();
  
  $(function () {
    MT.controller.initialize();
  });
  
// })(jQuery);