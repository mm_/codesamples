var mongoose = require('mongoose'),
    assert = require('assert'),
    Schema = require('mongoose').Schema;

var ObjectId = Schema.Types.ObjectId;

var userSchema = mongoose.Schema({
    name:{
        last:String,
        first:{ type:String, trim:true }
    },
    role:String
});
var projectSchema = mongoose.Schema({
    projectName:String,
    spreadsheetLink:String
});
var resourceSchema = mongoose.Schema({
    employeeId:String,
    name:{
        last:String,
        first:{ type:String, trim:true }
    },
    position:String,
    departmentId:ObjectId
});
var allotmentSchema = mongoose.Schema({
    projectId:ObjectId,
    resourceId:ObjectId,
    description:String,
    startDate:Date,
    endDate:Date,
    workDuration:Number
});
var departmentSchema = mongoose.Schema({
    departmentName:String
});

var Models = function () {
	this.projectSchema=projectSchema;
	this.resourceSchema=resourceSchema;
	this.allotmentSchema=allotmentSchema;
	this.departmentSchema=departmentSchema;
	this.userSchema=userSchema;
};

Models.prototype = {
    user:mongoose.model('users', userSchema),
    project:mongoose.model('projects', projectSchema),
    resource:mongoose.model('resources', resourceSchema),
    allotment:mongoose.model('allotments', allotmentSchema),
    department:mongoose.model('departments', departmentSchema)
};

module.exports = new Models();
