/*

                          .,.   .   .,,,,..
                     .xnMP"".xr`! nx.`""""""_..
                  .nPP"".,nMP" ;!;,.`""""""""""
                  .,nmMMPP" ,;!!!!!!!!!!!!!!!!!!;
                . ???"",;;!!!!!!!!!!!!!'''''''''<!
             .<!!;;;<!!!!!!!'''```_.,cccccc$$$c,`!
           ;<!!!!!!!!''``.,ccccd$$$$$$$$$$???$$$h.
          !!!!!!!!!!!> $$$$P""??$$$$$$$$P zcc "$$$
         !!!!!!!!!!!!>,$$P",c$$c,"?$$$$$ d$$$$c`$$
        ;!!!!!!!!!!!! d$$',$$$$$$c "$$$P $$$$$$c`$
        !!!!!!!!!!!!! $$$ $$$$$$$$h ?$$P $$$$$$$.`r
        !!!!!!!!!!!!! $$$ $$$$$$$$$h ?$> $$$$$$$$ $.
       ;!!!!!!!!!!!!! $$$ $$$$$$$$$$c`$>`$$$$???$.?$.
       !!!!!!!!!!!!!! $$$ ?$$$$$$???? $h $$$"    ?;$$.
       !!!!!!!!!!!!!! $$$.`$$$$$"     ?$ $$F      <$$$.
       !!!!!!!!!!!!!>,$$$$ ?$$$$      ?$.`$h      <$$$$.
       !!!!!!!!!!!!! <$$$$.`$$$$.     <$h $$hc,,. $???$$c,
       !!!!!!!!!!!!! $$$$$$.`$$$$c,,.r<$$r`?""`_. .zcc$$$$c
       ``!!!!!!!!!!! $$$$$$$ ??"""_..,,,cc  ???"",d$$$$$$$$h
          ```!!!!!!',$??$$P" ,c$$$$$$$$$$$$$c 4d$C,<$$$$$$$$
              !!!!! $" d$$cc$$$$$P". ,d$$$$$P"$$$P d$$$$$$$$
              `!!!'  z$$$$$$$$$$$,c$c ?$$$P". `"" d$$$$$$$$$
c             <!!! ,d$$$$$$$$$$$$$$$$c,`"`,mM MMP,$$$$$$$$P'
$           .<!!! z$$$$$$$$$$$$$$$$$$$$$c`MMM MM"d$$$$$$P"
$L      ,;<!!!!!  $$$$$$$$$$$$$$$$$$$$$$$ "MP P"z$$$$$P",;;,
$$.      ' ;!!!   $$$$$$$$$$$$$$$$$$$$$$$$c`"`,c$P""",;!!!!'
$$$.        `!    `?$$$$$$$$$$$$$$$$$$$$$$$$$$P"    `!!!''
$$$h                 .,.`````.,,,,,,,ccc,`""".z.     `'
$$$$h              ,c$$$$$$$$$$$$$$$$$$$$$$$c,`"$c,
$$$$$.           ,d$$$$$$$$$$$$$$$$$$$$$$$$$$$$c ?$$c
$$$$$h         ,c$$$$$",$$$$$$$$$$$$$$$$$$$$$$$$$ "$$$c
$$$$$$        z$$$$$$',$$$$$$$$$$$$$$$$$$$$$$$$$$$c`$$$h.
$$$$$$L      z$$$$$$',$$$$$$$$$$$$$$$$$P""""?$$$$$$.?$$$$.
$$$$$$$.    <$$$$$$',$$$$$$$$$$$$$$P".,cd$$h.`?$$$$h $$$$$
$$$$$$$h   ,$$$$$$',$$$$$$$$$$$$P" ,c$$$$."?$h ?$$$$."$$$$
$$$$$$$$c  `$$$$$$ $$$$$$$$$$$P",c$$$F"?$$$c ?>;$$$$h $$$$
$$$$$$$$$$h ?$$$$$."??$$$$$$P",d$$$$$$hc`"$$h ,d$$$$$ $$$F
$$$$$$$$$$$L "$$$$$hcc,`"??",c$$$$$$$$$$$c`$$>`$$$$$F,$$$
$$$$$$$$$$$$ c,"?$$$$$$$$c,d$$$$$P",c ?$$$ $$'d$$$$$';$$'
$$$$$$$$$$$$ $$c,`"??$$$$$$$$$$P",d$$h ?$$ $",$$$$$F,$$'
$$$$$$$$$$$$ $$$$$hc,_"""$$$P".z$$$$$$h."?  z$$$$$P d$,,,.,.
$$$$$$$$$$$$c`?$$$$$$$$$c,.,cd$$$$$$$$$$ccc$$$$$$P c??$$$$$$h
$$$$$$$$$$$$$c ?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$F z$c,.""???'
$$$$$$$$$$$$$$c,`?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" h,`?$$$$hcc
$$$$$$$$$$$$$$$$h.`?$$$$$$$$$??$$$$$$$$$$$$$P"    "?c,"""""
$$$$$$$$$$$$$$$$$$c,"?$$$$$$$c`$$$$$$$$$P""
"$$$$$$$$$$$$$$$$$$$h  ?$$$$$$ $$???""
  "?$$$$$$$$$$$$$$??"  `$$$$$$ $$$$$$
      ""??$???"""       $$$$$$ $$$$$$
                        $$$$$$.?$$$$$
                        $$$$$$>'$$$$$r
                        $$$$$$h`$$$$$$.
                       ,$$$$$$$ $$$$$$$$cc,.
                       d$$$$$$$ `$$$$$$$$$$$$hccc,.
                     ,d$$$$$$$$  `$$$$$$$$$$$$$$$$$$$cc,.
                   ,c$$$$$$$$$$   `$$$$$$$$$$$$$$$$$$$$$$c
                ,c$$$$$$$$$$$$F      `"??$$$$$$$$$$$$$$$$$h
             .zd$$$$$$$$$$$$$"             ""?????$$$$????'
           ,c$$$$$$$$$$$$$$"
         ,d$$$$$$$$$$$$$$"
        c$$$$$$$$$$$$$$"
       z$$$$$$$$$$$$P"
      ,$$$$$$$$??""
      `$$???""


*/
var 
	Google_Client		=	require('GoogleSDK/Google_Client').Google_Client,
	Google_DriveService	=	require('GoogleSDK/contrib/Google_DriveService'),
	Google_DriveFile	=	require('GoogleSDK/contrib/Google_DriveFile'),
	Google_HttpRequest	=	require('GoogleSDK/io/Google_HttpRequest'),
	Google_AssertionCredentials=	require('GoogleSDK/auth/Google_AssertionCredentials'),
	spreadsheets 		= 	require('./spreadsheets');
	config			= 	require('./config');
	CLIENT_ID		=	'',
	SERVICE_ACCOUNT_NAME	=	config.google.service_account_name,
	PEM_KEY_FILE		=	config.google.cert,
	APPLICATION_NAME	=	'',
	FILE_PREFIX		=	'Resourcing | ',
	RESOURCES_FILE_NAME	=	'Prolific | Company Resources';

var GoogleInputAdapter = function(callback){
/*
	Scopes: an array of different permissions request by the Application
*/
var scopes = new Array(
	"https://www.googleapis.com/auth/drive.file",
	"https://spreadsheets.google.com/feeds/",
	"https://www.googleapis.com/auth/drive",
	"https://www.googleapis.com/auth/userinfo.email",
	"https://www.googleapis.com/auth/userinfo.profile",
	"https://docs.googleusercontent.com/",
	"https://docs.google.com/feeds/");

/*
	fileId: the ID of the spreadsheet to be processeed
	There will be multiple spreadsheets, so we will need to process all the spreadsheets
	titled : Resourcing | [TITLE]
*/

this.options = { 
      serviceAccountName:	SERVICE_ACCOUNT_NAME,       
      scopes            :       scopes,
      privateKey        :       PEM_KEY_FILE,
      privateKeyPassword:       'notasecret',
      assertionType     :       'http://oauth.net/grant_type/jwt/1.0/bearer',
      prn               :       false
};  

this.started= false;
this.debug = false;
this.totalCount= 0;
this.Projects = [];//{projectName:,spreadsheetLink:}
this.Resources= [];//{employeeId:,firstName:,lastName:,department:}
this.Allotments= [];//{employeeId:,description:,startDate:,endDate:,workDuration:}
this.callbacks = [];

}
GoogleInputAdapter.prototype.load = function(type){
callback = null;
This = this;
var fileId = "";
if(this.started) return;
this.started = true;
var auth 	=	new Google_AssertionCredentials(this.options);
auth.generateAssertion(function(assertion){
	var client = new Google_Client();
	client.setAssertionCredentials(auth,assertion);
	var options = { 'client' : client };
	var DriveService = new Google_DriveService(options);
	DriveService.request('list',function(response){
		data = JSON.parse(response.getResponseBody());
		for(var i in data['items']){
			if(data['items'][i]['title'].indexOf(RESOURCES_FILE_NAME) == 0 ){
				This.totalCount++;
				fileId = data['items'][i]['id'];
				var  option = {
					key:fileId,
					auth:client.auth.token.access_token
				}; 
	var ppl = [];
	spreadsheets(option,function(err,spreadsheet){
		spreadsheet.worksheets[0].rows({
			start:1,
		},function(err,cells){
			var SpreadU = [];
			cells.forEach(function(cell){
/*
	Row 1 - IGNORE
	Row 2-N 
		Col1: Username
		Col2: First Name
		Col3: Last Name
		Col4: Department
		Col5: Position
*/
				row = cell['gs:cell'][0].$.row;
				col = cell['gs:cell'][0].$.col;
				value = cell['gs:cell'][0].$.inputValue;
				if(typeof SpreadU[row] == 'undefined')
					SpreadU[row] = [];
				value = cell['gs:cell'][0].$.inputValue;
				SpreadU[row][col] = value;

			});
			for(var g in SpreadU){
				if(g==1) continue;
				var Pers = {firstname:SpreadU[g][2],
						lastname:SpreadU[g][3],
						username:SpreadU[g][1],
						department:SpreadU[g][4],
						position:SpreadU[g][5]
						};
				ppl[Pers['username']] = Pers;
			}
			This.totalCount--;
		});
	});
			/*
				TODO:	
					READ EMPLOYEE SPREADSHEET AND 
					STORE IN ARRAY FOR FUNCTIONS BELOW
			*/
			
function getFirstName(employeeId){
	return ppl[employeeId]['firstname'];
}
function getLastName(employeeId){
	return ppl[employeeId]['lastname'];
}
function getDepartment(employeeId){
	return ppl[employeeId]['department'];
}
			}
			if(data['items'][i]['title'].indexOf(FILE_PREFIX) == 0 ){
				This.totalCount++;
				This.Projects.push({projectName:data['items'][i]['title'].replace(FILE_PREFIX,''),spreadsheetLink:data['items'][i]['alternateLink']});				
				This.Allotments[data['items'][i]['title'].replace(FILE_PREFIX,'')] = [];
				fileId = data['items'][i]['id'];
	//DriveService.request('get',function(response){
		//data = JSON.parse(response.getResponseBody());
		//file = new Google_DriveFile(data);
	//file.getId(), 
	var  option = {
				key:fileId,
				auth:client.auth.token.access_token
			}; 
	spreadsheets(option,function(err,spreadsheet){
	var Spread = [];
		spreadsheet.worksheets[0].rows({
			start:1,
		},function(err,cells){
			cells.forEach(function(cell){
				row = cell['gs:cell'][0].$.row;
				col = cell['gs:cell'][0].$.col;
				if(typeof Spread[row] == 'undefined')
					Spread[row] = [];
				value = cell['gs:cell'][0].$.inputValue;
				Spread[row][col] = value;
	/*	Format of the spreadsheet

		Row 1: Col 1-3 titles (not used)
		Row 1: Col 4-N employee names
		Rows 2-N: If Cols 2,3 are empty (undefined) It is a "Phase" Row and not used"
			if Cols 2,3 are not empty (dates) 
				Col 1: Task
				Col 2: Start Date
				Col 3: End Date
				Cols 4-N: Number of hours for employee found in Row1Col(N) 
	*/
			});
			columns = 0;
			for(var i in Spread[1]){
				if(i>=4)
				This.Resources.push ({employeeId:Spread[1][i],firstName:'',lastName:'',department:''});
				columns++;
			}
			for(var i in Spread){
				if(parseInt(i) > 1){
					if(Spread[i][2] == '' && Spread[i][3] == ''){//might be undefined
						continue;
					}else{
						var task = Spread[i][1];
						var startDate = Spread[i][2];
						var endDate = Spread[i][3];
						for(var j in Spread[i]){
							if(j>=4 && j <= columns){
								var workDuration = Spread[i][j];
								employeeId = Spread[1][j];
								This.Allotments[spreadsheet.title.replace(FILE_PREFIX,"")].push({employeeId:employeeId,description:task,startDate:startDate,endDate:endDate,workDuration:workDuration});				
							}
						}
					}
					
				}
			}
			This.totalCount--;
			if(This.totalCount == 0){
				for(var m in This.Resources){
					uid = This.Resources[m].employeeId;
					This.Resources[m].firstName=getFirstName(uid);
					This.Resources[m].lastName=getLastName(uid);
					This.Resources[m].department=getDepartment(uid);

				}
				for (var i in This.callbacks){
					callback=This.callbacks[i].fn;
					type=This.callbacks[i].type;
					switch(type){
						case "Projects":
							callback(This.Projects);
							break;
						case "Resources":
							callback(This.Projects);
							break;
						case "Allotments":
							callback(This.Allotments[This.callbacks[i].projectName]);
							break;
					}
				}
			This.callbacks = [];	
			This.started=false;
			}
			if(This.debug){
				console.log("Projects");
				console.log(This.Projects);
				console.log("Resources");
				console.log(This.Resources);
				console.log("Allotments");
				console.log(This.Allotments);
			}
		});
	});
			}
		}
	});
});

};



GoogleInputAdapter.prototype.getAllProjects = function (callback) {
	this.callbacks.push({type:"Projects",fn:callback});
	this.load();
};
GoogleInputAdapter.prototype.getAllResources = function (callback) {
	this.callbacks.push({type:"Resources",fn:callback});
	this.load();
};
GoogleInputAdapter.prototype.getAllotmentsForProject = function (projectName, callback) {
	this.callbacks.push({type:"Allotments",fn:callback,projectName:projectName});
	this.load();
};
module.exports = new GoogleInputAdapter();
