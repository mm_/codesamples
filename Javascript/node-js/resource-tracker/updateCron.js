var mongoose = require('mongoose'),
    docWriter = require('./docWriter'),
    assert = require('assert'),
    config = require('./config.json');

var execCount = 0;
docWriter.inputAdapter = require(config.inputAdapter);

var updateDB = function () {
    // Makes sure the processData function runs only once concurrently
    var oneAtATime = function () {
        execCount++;
        if (execCount == 1) {
            var updateComplete = function () {
                if (execCount > 0) {
                    execCount--;
                    docWriter.processData(updateComplete);

                    setTimeout(
                        oneAtATime,
                        5 * 60 * 1000 // 5 minutes
                    );
                }
            };

            docWriter.processData(updateComplete);

            setTimeout(
                oneAtATime,
                5 * 60 * 1000 // 5 minutes
            );
        }
    };

    oneAtATime();
};

mongoose.connect(config.mongo.host, config.mongo.database, function (err, res) {
    updateDB();
});
