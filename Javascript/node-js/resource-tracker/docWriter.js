var rtmodels = require('./models.js'),
    config = require('./config.json');

var DocWriter = function () {
};

// To use: set inputAdapter to a valid input adapter and then call processData.
DocWriter.prototype = {
    inputAdapter:null,

    // Reads projects from inputAdapter and writes them to mongo
    readProjects:function (callback) {
        this.inputAdapter.getAllProjects(function (projects) {
            var writeCount = 0;
            var writeDone = function () {
                writeCount--;
                if (writeCount == 0) {
                    callback();
                }
            };

            if (projects.length == 0) callback();
            projects.forEach(function (value, index, array1) {
                rtmodels.project.find({projectName:value.projectName}, function (err, obj) {
                    if (obj.length == 0) {
                        var project = new rtmodels.project({projectName:value.projectName, spreadsheetLink:value.spreadsheetLink});
                        writeCount++;
                        project.save(writeDone);
                    }
                    else if (projects.length - 1 == index && writeCount == 0) {
                        console.log('No New Projects');
                        callback();
                    }
                });
            });
        });
    },

    // Reads departments from inputAdapter and writes them to mongo (these are derived from resources)
    readDepartments:function (callback) {
        this.inputAdapter.getAllResources(function (resources) {
            var writeCount = 0;
            var writeDone = function () {
                writeCount--;
                if (writeCount == 0) {
                    callback();
                }
            };

            var uniqueDepartmentNames = new Array();
            resources.forEach(function (value, index, array1) {
                index = uniqueDepartmentNames.indexOf(value.department);
                if (index < 0) {
                    uniqueDepartmentNames.push(value.department);
                }
            });

            if (uniqueDepartmentNames.length == 0) callback();
            uniqueDepartmentNames.forEach(function (value, index, array1) {
                rtmodels.department.find({departmentName:value}, function (err, obj) {
                    if (obj.length == 0) {
                        var department = new rtmodels.department({departmentName:value});
                        writeCount++;
                        department.save(writeDone);
                    }
                    else if (uniqueDepartmentNames.length - 1 == index && writeCount == 0) {
                        console.log('No New Departments');
                        callback();
                    }
                });
            });
        });
    },

    // Reads resources from inputAdapter and writes them to mongo (departments must exist first)
    readResources:function (callback) {
        this.inputAdapter.getAllResources(function (resources) {
            var writeCount = 0;
            var writeDone = function () {
                writeCount--;
                if (writeCount == 0) {
                    callback();
                }
            };

            var writeResource = function (value, departmentId) {
                var resource = new rtmodels.resource({
                    employeeId:value.employeeId,
                    name:{first:value.firstName, last:value.lastName},
                    position:value.position,
                    departmentId:departmentId
                });
                writeCount++;
                resource.save(writeDone);
            };

            if (resources.length == 0) callback();
            resources.forEach(function (value, index, array1) {
                rtmodels.resource.find({employeeId:value.employeeId}, function (err, obj) {
                    if (obj.length == 0) {
                        rtmodels.department.find({departmentName:value.department}, function (err, obj) {
                            if (obj.length > 0) {
                                writeResource(value, obj[0]._id);
                            }
                        });
                    }
                    else if (resources.length - 1 == index && writeCount == 0) {
                        console.log('No New Resources');
                        callback();
                    }
                });
            });
        });
    },

    // Writes a set of allotments to a project. Allotments are overwritten by deleting all of the data and
    // inserting the new allotment data
    writeAllotmentsForProject:function (project, allotments, callback) {
        var finishedQueue = [];

        var writeDone = function () {
            if (finishedQueue.length == allotments.length) {
                callback();
            }
        };

        rtmodels.allotment.find({projectId:project._id}).remove(function () {
            if (allotments.length == 0) callback();
            allotments.forEach(function (value, index, array1) {
                rtmodels.resource.find({employeeId:value.employeeId}, function (err, obj) {
                    if (obj.length > 0) {
                        var allotment = new rtmodels.allotment({
                            projectId:project._id,
                            resourceId:obj[0]._id,
                            description:value.description,
                            startDate:value.startDate,
                            endDate:value.endDate,
                            workDuration:value.workDuration
                        });
                        allotment.save(function () {
                            finishedQueue.push(allotment);
                            writeDone();
                        });
                    }
                    else {
                        finishedQueue.push(value);
                        writeDone();
                    }
                });
            });
        });
    },

    // Reads allotments from inputAdapter and writes them to mongo (resources and projects must exist first)
    readAllotments:function (callback) {
        console.log('readAllotments started');

        var me = this;

        var writeCount = 0;
        var writeDone = function () {
            writeCount--;
            if (writeCount == 0) {
                callback();
            }
        };

        rtmodels.project.find(function (err, obj) {
            if (obj.length > 0) {
                obj.forEach(function (value, index, array1) {
                    var project = value;

                    console.log('Getting Allotments for ' + project.projectName);
                    me.inputAdapter.getAllotmentsForProject(project.projectName, function (allotments) {
                        writeCount++;
                        me.writeAllotmentsForProject(project, allotments, writeDone);
                    });
                });
            }
            else {
                callback();
            }
        });
    },

    // Main function that should be called for input adapter
    processData:function (callback) {
        console.log('Running update...');

        var me = this;

        // Maintains the required dependencies of the objects

        var readCount = 2;
        var readDone = function () {
            readCount--;
            if (readCount == 0) {
                me.readAllotments(callback);
            }
        };

        this.readDepartments(function () {
            me.readResources(readDone);
        });
        this.readProjects(readDone);
    }
};

module.exports = new DocWriter();
