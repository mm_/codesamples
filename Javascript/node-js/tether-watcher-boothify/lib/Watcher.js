var fs = require('fs'),
	_ = require('underscore'),
	EventEmitter = require('events').EventEmitter;
var exec = require('child_process').exec;
var Watcher = function (dir) {
	var _this = this;
	
	//Inherit EventEmitter members
	EventEmitter.call(this);
	
	//Private members
	this._dir = dir;
	this._first = true;
	this._printer= "";
	
	this.latest = {filename:"",mtime:""},
	this._oldLatest = _.extend({},this.latest),
	//Watch the directory
	//Debounce because image capture adds temporary file and renames it (fires twice)
	fs.watch(dir, 
		_.debounce(function () {
		_this._onFileEvent.apply(_this, arguments);
	}, 200));
	var files = fs.readdirSync(this._dir);
	this.checkFiles(files);
};
Watcher.prototype = {
	__proto__: EventEmitter.prototype,
	loadPrinter : function (){
		var _this = this;
		exec('lpstat -d | sed -e "s/system default destination: //"',function (error,stdout,stderr){
			_this._printer = stdout.replace(/\s+/g, '');;
				if(stderr.length){
				    console.log('stderr: ' + stderr);
				}
			    if (error !== null) {
			      console.log('exec error: ' + error);
			    }
			console.log("USING PRINTER: " + _this._printer);
		});
	},
	printFile: function(filename){
		if(!this._printer.length)
			this.loadPrinter();
		var command = 'lpr -P '+ this._printer + ' ' + filename;
		var command = 'lpr  ' + filename;
		console.log(command);
		exec(command , 
			function ( error,stdout,stderr){
				if(stderr.length){
				    console.log('stderr: ' + stderr);
				}
				    if (error !== null) {
				      console.log('exec error: ' + error);
				    }

			});

	}, 
	
	checkFiles: function(files){
		for(var i in files){
			if(files[i].match(/^\..*/) || !files[i].match(/^.*\.JPG$/)){
				console.log(files[i]);
				continue;
			}
			var stats = fs.statSync(this._dir+"/"+files[i]);
			this.checkLatest(stats,files[i]);
		}

	},
	checkLatest: function(stats,filename){
                        if(this.latest.mtime==''){
                               this.latest.filename = filename;
                               this.latest.mtime = stats.mtime;
				this._oldLatest = _.extend({}, this.latest);
                        }else{
                                if(Date.parse(this.latest.mtime) <= Date.parse(stats.mtime)){
					this._oldLatest = _.extend({}, this.latest);
                                        this.latest.filename = filename;
                                        this.latest.mtime = stats.mtime;
                                }
                        }

	},


	
	_onFileEvent: function (args) {
		var _this = this;
		var files = fs.readdirSync(this._dir);
		_this.checkFiles(files);
		if(this.latest.filename != this._oldLatest.filename) {
			if(_this.first)
				_this.first = false;
			else
				_this.emit('file', this.latest.filename);
		}
		
		/*fs.readdir(this._dir, function (err, files) {
			files.forEach(function (file) {
			});
		});
		*/
	}
};

module.exports = Watcher;
