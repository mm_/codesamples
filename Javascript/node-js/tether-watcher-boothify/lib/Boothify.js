var fs = require('fs'),
        _ = require('underscore'),
        EventEmitter = require('events').EventEmitter;
var Watcher = require('./Watcher');
var exec = require('child_process').exec;
var log = console.log;
var Boothify = function (dir,save_dir,template_dir,booth_id,prefix,suffix,debug){
        if(typeof debug == 'undefined')
                this._debug = false;
        else 
                this._debug = true;
        if(typeof prefix== 'undefined')
                this._file_prefix= "temp_";
        else 
                this._file_prefix= prefix;
        if(typeof suffix== 'undefined')
                this._file_suffix= ".jpg";
        else 
		this._Watcher = new Watcher(dir);
		this._Watcher.loadPrinter();
                this._file_suffix= suffix;
		var _this = this;
                this._directory = dir;
                this._save_dir = save_dir;
		this._template_dir = template_dir;
                this._booth_id = booth_id;
                this._url = "http://www.boothify.me/";
                this._uploadEndpoint = this._url+"?booth_id="+booth_id;
		this._file_count = 1;
		this._template_index = 0;
		this._border_color = "#3d7cd4";
		this._resize_size = "1500x1000";
		this._templates = [
                this._template_dir + "template.jpg" ,
                this._template_dir + "template2.jpg" ,
                this._template_dir + "template3.jpg" ,
                this._template_dir + "template4.jpg" ,
                this._template_dir + "template5.jpg" ,
                ];
}
Boothify.prototype = {
        __proto__: EventEmitter.prototype,
        uploadNewImage : function(imagefilename){
                var curl = 'curl -v -F "photos[]=@'+imagefilename+'" '+this._uploadEndpoint;
                if (this._debug) log(curl);
                return exec(curl,function(error,stdout,stderr){
                        if(this._debug)log(stdout);
                });

        },
	getNextFilename : function (directory,prefix) {
		if(typeof prefix ==='undefined')
			prefix = this._file_prefix;
		var filename = directory+prefix+this._file_count+this._file_suffix;
		try {
			stats = fs.existsSync(filename);
			if(!stats)
				{return filename;}
                else
                        {this._file_count++;return this.getNextFilename(directory,prefix);}
		}catch(e){log(e)}
	},
	resizeImage : function (original,resized,size,callback){
		_this = this;
		if(this._debug){
			log("Resizing image: "+original);
			log("Saving as ("+size+"): "+resized);
		}
		if(typeof size ==='undefined')
			size = this._resize_size;
		if (!fs.existsSync(original))
			return false;
		var command = 'convert -resize '+size+' \''+original+'\' \''+resized+"'";
		if(this._debug)
			log(command);
		return exec(command,
			function(error,stdout,stderr){
				if(_this._debug)
					log(stdout);
				if(typeof callback =='string')
					return _this.emit(callback,resized);
			}
		);
	},
	compositeImage : function (over,template,output_file,callback) {
		_this = this;
		var command = 'composite  -gravity center  '+over+' '+template+' '+output_file;
		if(this._debug)
			log(command);
		return exec(command,
			function(error,stdout,stderr){
					 if(_this._debug)
						 log(stdout);
					 if(typeof callback =='string')
						 return _this.emit(callback,output_file);
				 }
			);
	
	},
	addBorder : function(input,output,callback) {
		_this = this;
		var command = 'convert -border 3%x3% -bordercolor \''+_this._border_color+'\' '+input+' '+output;
		if(this._debug)
			log(command);
		return exec(command,
			function(error,stdout,stderr){
                      if(_this._debug)
			log(stdout);
			 if(typeof callback =='string')
				_this.emit(callback,output);
				 }
			    );
	},
	print : function (file){
		this._Watcher.printFile(file);

	},
	start : function (){
		var _this = this;
		this.on('compositeImage',function(file){
			var output_templated  = _this.getNextFilename(_this._save_dir,"templated_");
			var template = _this._templates[_this._template_index++];
			if(_this._template_index >= _this._templates.length)
				_this._template_index = 0;
			_this.compositeImage(file,template,output_templated,'addPadding');
		});
		this.on('startUpload',function(file){
			_this.uploadNewImage(file);
		});
		this.on('addPadding',function(file){ 
			var output_padded  = _this.getNextFilename(_this._save_dir,"final_");
			_this.addBorder(file,output_padded,'printFile');
		});
		this.on('printFile',function(file){ 
			_this.print(file);
			if(this._debug){
				log("");
				log("Done with image: "+_this._file_count+".");
				log("Using template # "+_this._template_index+".");
				log("");
				log("===============================<<NEXT>>=============================");
				log("");
			}
		});
		this._Watcher.on('file', function (file) {
				log("");
				log("===============================>>STARTING<<=============================");
				log("");
			var save = _this.getNextFilename(_this._save_dir);
			var filename = _this._directory + file;
			var output = _this.getNextFilename(_this._save_dir);
			var output_resized_s  = _this.getNextFilename(_this._save_dir,"small_");
			var output_resized_l  = _this.getNextFilename(_this._save_dir,"large_");
//then resize to uploadable size
			_this.resizeImage(filename,output_resized_s,"586x384",'startUpload');//,_this.uploadNextImage);
//first resize
			_this.resizeImage(filename,output_resized_l,_this._resize_size,'compositeImage');

//finally upload
		});
	}



}

module.exports = Boothify;
