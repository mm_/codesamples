#!/usr/bin/env node
var listen_directory = '/Users/matt/Documents/Boothify/NBC/Pictures/';
var save_directory = '/Users/matt/Documents/Boothify/NBC/boothify_print/';
var template_dir = '/Users/matt/Documents/Boothify/NBC/templates/';
//var template_dir = '/Users/matt/Desktop/templates/';
var templates = [ 
		template_dir + "template.jpg" , 
		template_dir + "template2.jpg" , 
		template_dir + "template3.jpg" , 
		template_dir + "template4.jpg" , 
		template_dir + "template5.jpg" , 
		]
var Boothify = require('./lib/Boothify');
var http = require('http');
var fs = require('fs');
var log = console.log;
var template_index = 0;
var booth_id=25;
var file_prefix = "nbcu_";
var file_suffix = ".jpg";
var debug = false;

var boothify = new Boothify(listen_directory,
				save_directory,
				template_dir,
				booth_id,
				file_prefix,
				file_suffix,
				debug);

boothify.start();
