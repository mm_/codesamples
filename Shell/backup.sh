#/bin/bash

HOST=$1
PORT=$2
USER=$3
KEY_FILE=$4
MYSQL_USER=$5
MYSQL_PASSWORD=$6
MYSQL_DATABASE=$7
#default
TUNNEL_PORT=9999
TUNNEL_SLEEP=5
LOCALHOST="localhost"
REMOTE_PORT=$PORT
REMOTE_SERVER_IP=$HOST
REMOTE_SERVER_USER=$USER
REMOTE_SERVER_KEY=$KEY_FILE
MYSQL_DUMP_DIR="/mysql_backups/"
FILE="asd"

function generate_random_file_name { 
#check MYSQL_DUMP_DIR if filename already exists
FILE="asdasda"
}
function get_remote_db {
        echo creating ssh tunnel $TUNNEL_PORT:$LOCALHOST:$REMOTE_PORT
        ssh -f -L$TUNNEL_PORT:localhost:$REMOTE_PORT $REMOTE_SERVER_USER@$REMOTE_SERVER_IP -i $REMOTE_SERVER_KEY sleep $TUNNEL_SLEEP
# dump through tunnel
        echo starting dump
# check if mysql_database is set otherwise set to --all-databases
        mysqldump -P $TUNNEL_PORT -h $LOCALHOST -u$MYSQL_USER -p$MYSQL_PASSW0RD --all-databases > $MYSQL_DUMP_DIR$FILE
        echo dump complete: $MYSQL_DUMP_DIR$FILE
# release tunnel port after process
}
