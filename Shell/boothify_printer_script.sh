#!/bin/bash
BOOTH_ID="26";
URL="http://boothify.me/helpers/tools.php?iid="
POLL_URL="http://boothify.me/helpers/tools.php?poll=true&booth_id=${BOOTH_ID}"
#PRINTER="Brother_MFC_7860DW"
#PRINTER="Canon_iP100_series"
#PRINTER="Canon_iP100_series"
PRINTER="`lpstat -d | sed -e "s/system default destination: //"`"
SLEEP_TIME="5s"

Functions=('print_single' 'start_polling' 'show_current_printer' 'change_printer' 'exit_')
Function_descriptions=('Print Single Photo Strip' 'Start Polling' 'Show Current Printer' 'Change Printer' 'Exit')
clear

echo -e '.\n'
sleep .20s
echo -e '..\n'
sleep .20s
echo -e '...\n'
sleep .20s
echo -e '....\n'
sleep .20s
echo -e '.....\n'
sleep .20s
echo -e "Welcome to Boothify!\n"
sleep .50s
echo -e "Using System Default Printer: $PRINTER\n"
sleep 2s

function show_menu { 
	clear
	i=${#Functions[@]}
	echo -n -e 'Commands [n]:\n';
	for ((k=0;k<$i;k++));
	do
		echo "[${k}]: "${Function_descriptions[${k}]}
	done

	echo -n Enter Command '> ' 
	read func
	ISF="`type -t ${Functions[${func}]}`"
	if [ "$ISF" != "function" ]; then
	       exit 2
	fi
	${Functions[${func}]}
}
function show_current_printer {
	echo -e "Using System Default Printer: $PRINTER\n"
	sleep 5s
	show_menu
}

#if [  $# -eq 1 ]; then
#print_single
#fi
function change_printer(){
	clear
	echo -n Enter Printer Name '> '
	read PRINTER
	show_menu
}


function exit_(){
	clear
	exit 2
}

function print_single(){
	clear
	echo Enter Photogroup ID# '> ' 
	read pid 
	echo -e "PRINTING $pid ..........\n\n"
	#say Printing
	`curl -s "$URL$pid" -o image.jpg`
	p=` lpr -o PageSize=Custom.4x6in -o MediaType=GLOSSY -P $PRINTER image.jpg `
	show_menu
}

function start_polling() { 
	i=0;
	while [ true ]
	do
	#echo $i
	i=$i+1
	c=`curl "$POLL_URL"`
	if [ "$c" == "0" ]; then
	echo not printing..
	#exit
	sleep 1
	else

	if [ $c -gt 0 ]; then
	echo -e "printing $c \n\n"
	say "Printing"
	`curl  "$URL$c" -o image.jpg`
	p=` lpr -o PageSize=Custom.4x6in -o MediaType=GLOSSY -P $PRINTER image.jpg `
	fi
	fi    
	sleep $SLEEP_TIME
	done
}

show_menu
