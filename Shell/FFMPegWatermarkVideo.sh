#!/bin/bash
ORIGINAL_VIDEO="/path/to/file.mov"
WATERMARK_IMAGE="/path/to/logo.png"
WATERMARKED_VIDEO="/path/to/file_output.mov"

function installFFMPEG {
#check svn installed
	svn co svn://svn.ffmpeg.org/soc/libavfilter
	cd libavfilter 
	./checkout.sh
	checkout libavfilter
	cd into libavfilter
	./configure --enable-avfilter --enable-libmp3lame --enable-shared --disable-mmx --arch=x86_64 --enable-libfaac --enable-nonfree --enable-filter=movie --enable-avfilter-lavf --enable-libx264 --enable-gpl
	make
	make install
	echo "/usr/local/lib/" > /etc/ld.so.conf.d/ffmpeg.conf
}
function addWatermark {
	if [ -z "$1" ]
	then
		exit
	fi
	if [ -z "$2" ]
	then
		exit
	fi
	if [ -z "$3" ]
	then
		exit
	fi
	input=$1
	image=$2
	output=$3

	result=`ffmpeg -y -i $input -f mp4 -sameq -vhook '/usr/local/lib/vhook/watermark.so -f '$image' ' $output`
	return $result
}
function convertFLV {
	if [ -z "$1" ]
	then
		exit
	fi
	if [ -z "$2" ]
	then
		exit
	fi
	input=$1
	output=$2
	result=`ffmpeg -i $input -f flv -b 200000 $output`
	return $result
}
addWatermark $ORIGINAL_VIDEO $WATERMARK_IMAGE $WATERMARKED_VIDEO
