# Code Samples

### by: Matt Margolin 


Last updated: April 30, 2014

---

### PHP

---

* Apple Push Notifications Class 
	(apple-push-notification.php)
  
* Magento Ajax Cart Module
	(magento-ajaxcart)

* Magento Custom Admin Panel
    (magento-custom-admin-panel)


### Javascript

--- 

* Backbone.js MVC Admin Panel
	(core/admin.js)
	
* Magento Ajaxcart 
	(core/ajaxcart.js)

* Magento Passport (Gamification Rewards Badges)
	(core/passport.js)
	
* Magento Prototype.js Shipping State Restriction
	(core/states.js)
	
 


### Ruby

---

* Muby Tool
	(muby/muby_app.rb & various modules located in muby/modules)



### Perl

---

* ThreadedRunner 
	(ThreadedRunnerSample.pl)

* ProxyListValidator
	(perl ProxyListValidatorExample.pl)


### Shell

---

* FFMpeg Videos Watermarking Script
	(FFMPegWatermarkVideo.sh)
	
* MySQL Remote Database Dump Retrieval Through SSH Tunnel
	(backup.sh)
	
* Boothify.me Event Templated Image Printer Script
	(boothify_printer_script.sh) 

### Various Configuration Examples

--- 

* Terminal Bash Config
	(Terminal_Bashrc)

* Monit Configuration
	(monit.conf)
	
* Vim Configuration
	(vim/)

(More to Come)

